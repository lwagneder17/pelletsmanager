import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './pages/login/login-page.component';
import { OrderPageComponent } from './pages/orders/orders-page/orders-page.component';
import { SupplierPageComponent } from './pages/suppliers/supplier-page/supplier-page.component';
import { MemberDetailsPageComponent } from './pages/member-details/member-details-page/member-details-page.component';
import { MembersPageComponent } from './pages/members/members-page/members-page.component'
import { ProjectDetailsPageComponent } from './pages/project-details/project-details-page/project-details-page.component';
import { ProjectsPageComponent } from './pages/projects/projects-page/projects-page.component';
import { OrderDetailsPageComponent } from './pages/order-details/order-details-page/order-details-page.component';
import { AuthenticationGuard } from './core/authentication.guard';
import { SupplierDetailsPageComponent } from './pages/supplier-details/supplier-details-page/supplier-details-page.component';
import { UsersPageComponent } from './pages/users/users-page/users-page.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'members'},
  { path: 'members', component: MembersPageComponent, canActivate: [AuthenticationGuard]},
  { path: 'login', component: LoginPageComponent},
  { path: 'orders', component: OrderPageComponent, canActivate: [AuthenticationGuard]},
  { path: 'suppliers', component: SupplierPageComponent, canActivate: [AuthenticationGuard]},
  { path: 'orders/order/:id', component: OrderDetailsPageComponent, canActivate: [AuthenticationGuard]},
  { path: 'suppliers/supplier/:id', component: SupplierDetailsPageComponent, canActivate: [AuthenticationGuard]},
  { path: 'members/member/:id', component: MemberDetailsPageComponent, canActivate: [AuthenticationGuard]},
  { path: 'projects', component: ProjectsPageComponent, canActivate: [AuthenticationGuard]},
  { path: 'projects/project/:id', component: ProjectDetailsPageComponent, canActivate: [AuthenticationGuard]},
  { path: 'users', component: UsersPageComponent, canActivate: [AuthenticationGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }