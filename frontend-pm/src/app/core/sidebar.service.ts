import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SidebarService {
  private sidebarStatus = new Subject<boolean>();

  public notify(status: boolean): void {
    this.sidebarStatus.next(status);
  }
  public listen(): Observable<boolean> {
    return this.sidebarStatus.asObservable();
  }
}
