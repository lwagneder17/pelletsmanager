import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ShortMemberDto } from '../models/ShortMemberDto';
import { ContactTypeDto } from '../models/ContactTypeDto';
import { AddMemberDto } from '../models/AddMemberDto';
import { LoginDto } from '../models/LoginDto';
import { AddSupplierDto } from '../models/AddSupplierDto';
import { DetailedOrderDto } from '../models/DetailedOrderDto';
import { EditOrderDto } from '../models/EditOrderDto';
import { DetailedMemberDto } from '../models/DetailedMemberDto';
import { EditMemberDto } from '../models/EditMemberDto';
import { ShortOrderDto } from '../models/ShortOrderDto';
import { OrderTypeDto } from '../models/OrderTypeDto';
import { ShortProjectSupplierDto } from '../models/ShortProjectSupplierDto';
import { AddOrderDto } from '../models/AddOrderDto';
import { ShortProjectDto } from '../models/ShortProjectDto';
import { AddProjectDto } from '../models/AddProjectDto';
import { DetailedProjectDto } from '../models/DetailedProjectDto';
import { EditProjectDto } from '../models/EditProjectDto';
import { DetailedProjectSupplierDto } from '../models/DetailedProjectSupplierDto';
import { SupplierDto } from '../models/SupplierDto';
import { AddProjectSupplierDto } from '../models/AddProjectSupplierDto';
import { EditProjectSupplierDto } from '../models/EditProjectSuppliersDto';
import { ProjectSupplierStatsDto } from '../models/ProjectSupplierStatsDto';
import { ContactTypeStatsDto } from '../models/ContactTypeStatsDto';
import { OrderTypeStatsDto } from '../models/OrderTypeStatsDto';
import { UserDto } from '../models/UserDto';
import { MemberPage } from '../models/MemberPage';
import { OrderPage } from '../models/OrderPage';
import { ProjectPage } from '../models/ProjectPage';
import { SupplierPage } from '../models/SupplierPage';
import { UserPage } from '../models/UserPage';
import { query } from '@angular/animations';
import { AddUserDto } from '../models/AddUserDto';
import { EditUserDto } from '../models/EditUserDto';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  baseUrl = 'http://localhost:8080/pelletsBackend';

  getMembers(_pageSize?: number, _pageNr?: number, _filterId?: number, _searchId?: number, _searchString?: string) {
    let queryParams = new HttpParams();
    if (_pageSize) queryParams = queryParams.append("pageSize", _pageSize);
    if (_pageNr) queryParams = queryParams.append("pageNr", _pageNr);
    if (_filterId) queryParams = queryParams.append("filterOption", _filterId);
    if (_searchId) queryParams = queryParams.append("searchOption", _searchId);
    if (_searchString) queryParams = queryParams.append("searchString", _searchString);

    return this.http.get<MemberPage>(`${this.baseUrl}/members`, { params: queryParams });
  }

  getMemberById(memberId: number) {
    return this.http.get<DetailedMemberDto>(`${this.baseUrl}/members/member/${memberId}`);
  }

  getMemberFilterOptions() {
    return this.http.get<string[]>(`${this.baseUrl}/members/filterOptions`);
  }

  getMembersBySearch(urlAddition: string) {
    return this.http.get<ShortMemberDto[]>(`${this.baseUrl}${urlAddition}`);
  }

  getMembersByFilter(filterOptionId: number) {
    return this.http.get<ShortMemberDto[]>(`${this.baseUrl}/members/membersByFilterOption/${filterOptionId}`);
  }

  getContactTypes() {
    return this.http.get<ContactTypeDto[]>(`${this.baseUrl}/contactTypes`);
  }

  addMember(member: AddMemberDto) {
    return this.http.post<ShortMemberDto>(`${this.baseUrl}/members`, member);
  }

  deleteMember(memberId: number) {
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.http.delete(`${this.baseUrl}/members/member/${memberId}`, { headers, responseType: 'text' });
  }

  getUserValidation(userData: LoginDto) {
    return this.http.post<UserDto>(`${this.baseUrl}/login`, userData);
  }

  getOrders(_searchString?: string, _pageNr?: number, _pageSize?: number) {
    let queryParams = new HttpParams();

    if (_pageSize) queryParams = queryParams.append("pageSize", _pageSize);
    if (_pageNr) queryParams = queryParams.append("pageNr", _pageNr);
    if (_searchString) queryParams = queryParams.append("searchString", _searchString);

    return this.http.get<OrderPage>(`${this.baseUrl}/orders`, {params: queryParams});
  }

  addOrder(order: AddOrderDto) {
    return this.http.post<ShortOrderDto>(`${this.baseUrl}/orders`, order);
  }

  deleteOrder(orderId: number) {
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.http.delete(`${this.baseUrl}/orders/order/${orderId}`, { headers, responseType: 'text' });
  }

  getSupplierFilterOptions() {
    return this.http.get<string[]>(`${this.baseUrl}/suppliers/filterOptions`);
  }

  getSuppliersBySearch(urlAddition: string) {
    return this.http.get<SupplierDto[]>(`${this.baseUrl}${urlAddition}`);
  }

  getSuppliersByFilter(filterOptionId: number) {
    return this.http.get<SupplierDto[]>(`${this.baseUrl}/suppliers/suppliersByFilterOption/${filterOptionId}`);
  }

  addSupplier(supplier: AddSupplierDto) {
    return this.http.post<SupplierDto>(`${this.baseUrl}/suppliers`, supplier);
  }

  deleteSupplier(supplierId: number) {
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.http.delete(`${this.baseUrl}/suppliers/supplier/${supplierId}`, { headers, responseType: 'text' });
  }

  getOrderTypes() {
    return this.http.get<OrderTypeDto[]>(`${this.baseUrl}/ordertypes`);
  }

  getOrderById(orderId: number) {
    return this.http.get<DetailedOrderDto>(`${this.baseUrl}/orders/order/${orderId}`);
  }

  getSupplierById(supplierId: number) {
    return this.http.get<SupplierDto>(`${this.baseUrl}/suppliers/supplier/${supplierId}`);
  }

  editOrder(order: EditOrderDto, orderId: number) {
    return this.http.put<DetailedOrderDto>(`${this.baseUrl}/orders/order/${orderId}`, order);
  }

  editSupplier(supplier: SupplierDto, supplierId: number) {
    return this.http.put<SupplierDto>(`${this.baseUrl}/suppliers/supplier/${supplierId}`, supplier);
  }

  editMember(member: EditMemberDto, memberId: number) {
    return this.http.put<DetailedMemberDto>(`${this.baseUrl}/members/member/${memberId}`, member);
  }

  getOrdersOfMember(memberId: number) {
    return this.http.get<ShortOrderDto[]>(`${this.baseUrl}/members/member/${memberId}/orders`);
  }

  getProjectSuppliers() {
    return this.http.get<ShortProjectSupplierDto[]>(`${this.baseUrl}/projectsuppliers`);
  }

  getAvailableDeliveryDates(projectSupplierId: number) {
    return this.http.get<string[]>(`${this.baseUrl}/availabledeliverydates/${projectSupplierId}`);
  }

  getProjects(_pageSize?: number, _pageNr?: number, _searchString?: string) {
    let queryParams = new HttpParams();

    if (_pageSize) queryParams = queryParams.append("pageSize", _pageSize);
    if (_pageNr) queryParams = queryParams.append("pageNr", _pageNr);
    if (_searchString) queryParams = queryParams.append("searchString", _searchString);

    return this.http.get<ProjectPage>(`${this.baseUrl}/projects`, {params: queryParams});
  }

  deleteProject(projectId: number) {
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.http.delete(`${this.baseUrl}/projects/project/${projectId}`, { headers, responseType: 'text' });
  }

  addProject(newProject: AddProjectDto) {
    return this.http.post<ShortProjectDto>(`${this.baseUrl}/projects`, newProject);
  }

  getProject(projectId: number) {
    return this.http.get<DetailedProjectDto>(`${this.baseUrl}/projects/project/${projectId}`);
  }

  updateProject(editProject: EditProjectDto, projectId: number) {
    return this.http.put<DetailedProjectDto>(`${this.baseUrl}/projects/project/${projectId}`, editProject);
  }

  getProjectSupplier(projectSupplierId: number) {
    return this.http.get<DetailedProjectSupplierDto>(`${this.baseUrl}/projectsuppliers/projectsupplier/${projectSupplierId}`);
  }

  deleteProjectSupplier(projectSupplierId: number) {
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.http.delete(`${this.baseUrl}/projectsuppliers/projectsupplier/${projectSupplierId}`, { headers, responseType: 'text' });
  }

  getAvailableSuppliersForProject(projectId: number) {
    return this.http.get<SupplierDto[]>(`${this.baseUrl}/project/${projectId}/availableSuppliers`);
  }

  addProjectSupplier(newProjectSupplier: AddProjectSupplierDto) {
    return this.http.post<DetailedProjectSupplierDto>(`${this.baseUrl}/projectsuppliers`, newProjectSupplier);
  }

  getSuppliers(_pageSize?: number, _pageNr?: number, _searchString?: string) {
    let queryParams = new HttpParams();

    if (_pageSize) queryParams = queryParams.append("pageSize", _pageSize);
    if (_pageNr) queryParams = queryParams.append("pageNr", _pageNr);
    if (_searchString) queryParams = queryParams.append("searchString", _searchString);

    return this.http.get<SupplierPage>(`${this.baseUrl}/suppliers`, {params: queryParams});
  }

  editProjectSupplier(projectSupplierId: number, editProjectSupplier: EditProjectSupplierDto) {
    return this.http.put<DetailedProjectSupplierDto>(`${this.baseUrl}/projectsuppliers/projectsupplier/${projectSupplierId}`, editProjectSupplier);
  }

  getProjectSupplierStats(projectId: number) {
    return this.http.get<ProjectSupplierStatsDto[]>(`${this.baseUrl}/project/${projectId}/projectSupplierStats`);
  }

  getContactTypeStats(projectId: number) {
    return this.http.get<ContactTypeStatsDto[]>(`${this.baseUrl}/project/${projectId}/contactTypeStats`);
  }

  getOrderTypeStats(projectId: number) {
    return this.http.get<OrderTypeStatsDto[]>(`${this.baseUrl}/project/${projectId}/orderTypeStats`);
  }

  getUsers(_pageSize?: number, _pageNr?: number){
    let queryParams = new HttpParams();

    if(_pageSize) queryParams.append("pageSize", _pageSize);
    if(_pageNr) queryParams.append("pageNr", _pageNr);

    return this.http.get<UserPage>(`${this.baseUrl}/users`, {params: queryParams});
  }

  addUser(newUser: AddUserDto, _pageSize?: number, _pageNr?: number){
    let queryParams = new HttpParams();

    if(_pageSize) queryParams.append("pageSize", _pageSize);
    if(_pageNr) queryParams.append("pageNr", _pageNr);

    return this.http.post<UserPage>(`${this.baseUrl}/users`, newUser, {params: queryParams});
  }

  deleteUser(userId: number, _pageSize?: number, _pageNr?: number){
    let queryParams = new HttpParams();

    if(_pageSize) queryParams.append("pageSize", _pageSize);
    if(_pageNr) queryParams.append("pageNr", _pageNr);

    return this.http.delete<UserPage>(`${this.baseUrl}/users/user/${userId}`, {params: queryParams});
  }

  editUser(userId: number, editUser: EditUserDto){
    return this.http.put<UserDto>(`${this.baseUrl}/users/user/${userId}`, editUser);
  }
}
