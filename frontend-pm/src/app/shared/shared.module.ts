import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberPipe } from './pipes/number.pipe';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';
import { AddOrderComponent } from './components/add-order/add-order.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    NumberPipe,
    SidebarComponent,
    AddOrderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports: [
    NumberPipe,
    SidebarComponent,
    AddOrderComponent,
    FooterComponent
  ]
})
export class SharedModule { }
