import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'localNumberFormat'
})
export class NumberPipe implements PipeTransform {

  transform(value: number): string {
    if(value){
      return new Intl.NumberFormat('de-DE').format(Number(value.toFixed(2)));
    }
    return "0,00";
  }

}
