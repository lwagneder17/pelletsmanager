import { Component, OnInit } from '@angular/core';
import {
  faBars,
  faUser,
  faUsers,
  faBriefcase,
  faFolderOpen,
  faUserClock,
} from '@fortawesome/free-solid-svg-icons';
import { SidebarService } from 'src/app/core/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  faBars = faBars;
  faUser = faUser;
  faUsers = faUsers;
  faBriefcase = faBriefcase;
  faFolderOpen = faFolderOpen;
  faSupplier = faUserClock;
  isSidebarCollapsed = false;
  currentUser: string | null = '';

  constructor(private sidebarService: SidebarService) {
  }

  ngOnInit(): void {
    this.currentUser = localStorage!.getItem('userShortForm');
  }

  collapseSidebar() {
    this.isSidebarCollapsed = !this.isSidebarCollapsed;
    this.sidebarService.notify(this.isSidebarCollapsed);
  }
}
