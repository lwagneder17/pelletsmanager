import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit, OnChanges {

  constructor() { }

  @Output() pageChanged = new EventEmitter<number>();
  @Input() count: number = 0;
  @Input() pageSize: number = 0;

  amountOfPages = 0;
  pages: number[] = [];
  currentPages: number[] = [];
  currentPage: number = 1;

  backButtonClass = "disabled no-pointer";
  forwardButtonClass = "disabled no-pointer";

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['count']){
      this.amountOfPages = Math.floor(this.count / this.pageSize);
      this.amountOfPages += this.count % this.pageSize > 0 ? 1 : 0;
      
      if(this.amountOfPages === 0){
        this.amountOfPages = 1;
      }

      this.pages = Array(this.amountOfPages).fill(1).map((x,i)=>x+i);

      this.checkNavigationButtons();
      this.calcCurrentPages();
    }
  }

  navigateTo(page: number){
    if(this.currentPage !== page){
      this.currentPage = page;

      this.checkNavigationButtons();
      this.calcCurrentPages();
      this.pageChanged.emit(this.currentPage);
    }
  }

  navButtonClicked(navigateTo: number){
    if(this.currentPage + navigateTo > 0 && this.currentPage + navigateTo <= this.amountOfPages){
      this.currentPage += navigateTo;

      this.checkNavigationButtons();
      this.calcCurrentPages();
      this.pageChanged.emit(this.currentPage);
    }
  }

  calcCurrentPages(){
    if(this.amountOfPages <= 3 || this.currentPage === 1){
      this.currentPages = this.pages.slice(0, 3);
    }
    else if(this.currentPage !== this.amountOfPages){
      this.currentPages = this.pages.slice(this.currentPage - 2, this.currentPage + 1);
    }
  }

  checkNavigationButtons(){
    this.backButtonClass = "disabled no-pointer";
    this.forwardButtonClass = "disabled no-pointer";

    if(1 < this.currentPage && this.currentPage < this.amountOfPages){
      this.backButtonClass = "";
      this.forwardButtonClass = "";
    }
    else if(1 < this.currentPage && this.currentPage === this.amountOfPages){
      this.backButtonClass = "";
    }
    else if(this.currentPage < this.amountOfPages && this.currentPage === 1){
      this.forwardButtonClass = "";
    }
  }

}
