import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ShortOrderDto } from 'src/app/models/ShortOrderDto';

@Component({
  selector: 'app-member-orders',
  templateUrl: './member-orders.component.html',
  styleUrls: ['./member-orders.component.css']
})
export class MemberOrdersComponent implements OnInit {

@Output() selectionChanged = new EventEmitter<number>();
@Input() orders: ShortOrderDto[] = [];

selectedOrderId = 0;
isEditOrderVisible = false;
editOrderId = 0;

  constructor() { }

  ngOnInit(): void {
  }

  orderSelected(orderId: number){
    if(this.selectedOrderId == orderId){
      this.selectedOrderId = -1;
    }
    else{
      this.selectedOrderId = orderId;
    }
    this.selectionChanged.emit(this.selectedOrderId);
  }

  editOrder(orderId: number){
    this.editOrderId = orderId;
    console.log(`editOrderId: ${this.editOrderId}`);
  }

}
