import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { RestService } from 'src/app/core/rest.service';
import { AddOrderDto } from 'src/app/models/AddOrderDto';
import { ContactTypeDto } from 'src/app/models/ContactTypeDto';
import { MemberPage } from 'src/app/models/MemberPage';
import { OrderTypeDto } from 'src/app/models/OrderTypeDto';
import { ShortMemberDto } from 'src/app/models/ShortMemberDto';
import { ShortProjectSupplierDto } from 'src/app/models/ShortProjectSupplierDto';

@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.css']
})
export class AddOrderComponent implements OnInit {

  addOrderForm: FormGroup = new FormGroup({});

  orderTypes: OrderTypeDto[] = [];
  members: ShortMemberDto[] = [];
  contactTypes: ContactTypeDto[] = [];
  suppliers: ShortProjectSupplierDto[] = [];
  availableDelDates: string[] = [];

  selectedSupplier: ShortProjectSupplierDto = {} as ShortProjectSupplierDto;

  newOrder: AddOrderDto = {} as AddOrderDto;

  @Output() modalClosed = new EventEmitter();
  @Output() orderAdded = new EventEmitter<AddOrderDto>();
  @Input() memberId: number = -1;
  @Input() isAddOrderVisible: boolean = false;

  constructor(private restService: RestService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.addOrderForm = this.formBuilder.group({
      memberId: new FormControl('', Validators.required),
      orderTypeId: new FormControl('', Validators.required),
      contactTypeId: new FormControl('', Validators.required),
      projectSupplierId: new FormControl('', Validators.required),
      amountOrdered: new FormControl(''),
      wishedDelDate: new FormControl(''),
      shortInfo: new FormControl('')
    });

    forkJoin(
      this.restService.getContactTypes(),
      this.restService.getMembers(),
      this.restService.getOrderTypes(),
      this.restService.getProjectSuppliers(),
    ).subscribe(([contactTypesResult, membersResult, orderTypesResult, projectSuppliersResult]) => {
      this.contactTypes = contactTypesResult;
      this.members = membersResult.pageMembers;
      this.orderTypes = orderTypesResult;
      this.suppliers = projectSuppliersResult;

      this.addOrderForm.controls['memberId'].setValue(this.memberId);
      console.table(this.addOrderForm.controls['memberId'].value);
    });

    this.addOrderForm.controls['projectSupplierId'].valueChanges.subscribe((selectedSupplierId: number) => {
      this.selectedSupplier = this.suppliers[this.suppliers.findIndex(x => x.id === selectedSupplierId)];
      this.restService.getAvailableDeliveryDates(selectedSupplierId)
      .subscribe(result => this.availableDelDates = result);
    });
  }

  closeModal(){
    this.modalClosed.emit();
  }

  onSubmit(){
    this.newOrder = this.addOrderForm.value;
    this.newOrder.userId = 1;
    this.orderAdded.emit(this.newOrder);
    this.modalClosed.emit();
  }

  get member() {return this.addOrderForm.controls['memberId'];}
  get orderType() {return this.addOrderForm.controls['orderTypeId'];}
  get contactType() {return this.addOrderForm.controls['contactTypeId'];}
  get projectSupplier() {return this.addOrderForm.controls['projectSupplierId'];}
  get amountOrdered() {return this.addOrderForm.controls['amountOrdered'];}
  get wishedDelDate() {return this.addOrderForm.controls['wishedDelDate'];}
  get shortInfo() {return this.addOrderForm.controls['shortInfo'];}
}
