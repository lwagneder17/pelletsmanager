import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { forkJoin } from 'rxjs';
import { RestService } from 'src/app/core/rest.service';
import { AddOrderDto } from 'src/app/models/AddOrderDto';
import { ContactTypeDto } from 'src/app/models/ContactTypeDto';
import { DetailedMemberDto } from 'src/app/models/DetailedMemberDto';
import { EditMemberDto } from 'src/app/models/EditMemberDto';
import { ShortOrderDto } from 'src/app/models/ShortOrderDto';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css'],
})
export class MemberDetailComponent implements OnInit, OnChanges {
  @Input() memberId: number = -1;

  member: DetailedMemberDto = {} as DetailedMemberDto;
  editMemberForm: FormGroup = new FormGroup({});
  editMember: EditMemberDto = {} as EditMemberDto;
  contactTypes: ContactTypeDto[] = [];
  memberOrders: ShortOrderDto[] = [];
  selectedOrderId = -1;

  isAddOrderVisible = false;
  isDeleteOrderVisible = false;

  constructor(
    private formBuilder: FormBuilder,
    private restService: RestService
  ) {}

  ngOnInit(): void {
    this.editMemberForm = this.formBuilder.group({
      title: new FormControl(''),
      additionalTitle: new FormControl(''),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      plz: new FormControl('', [
        Validators.required,
        Validators.maxLength(5),
        Validators.minLength(4),
      ]),
      city: new FormControl('', Validators.required),
      street: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      contactTypeId: new FormControl('', Validators.required),
      deliveryAddress: new FormControl(''),
      privateNumber: new FormControl('', Validators.required),
      secondaryNumber: new FormControl(''),
      internInfo: new FormControl(''),
      permanentInfo: new FormControl(''),
      active: new FormControl(''),
      password: new FormControl('', Validators.required),
      webId: new FormControl(''),
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.memberId != -1) {
      forkJoin(
        this.restService.getMemberById(this.memberId),
        this.restService.getContactTypes(),
      ).subscribe(([memberResult, contactTypesResult]) => {
        this.member = memberResult;
        console.table(this.member);
        this.memberOrders = this.member.orders;

        this.contactTypes = contactTypesResult;
        this.setFormValues();
      });
    }
  }

  onSubmit() {
    this.editMember = this.editMemberForm.value;

    this.restService
      .editMember(this.editMember, this.memberId)
      .subscribe((result) => {
        this.member = result;
        this.setFormValues();
      });
  }

  resetForm() {
    this.setFormValues();
  }

  closeDeleteModal(){
    this.isDeleteOrderVisible = false;
  }

  openDeleteOrderModal(){
    console.log('openDeleteModal!!!');
    this.isDeleteOrderVisible = true;
  }

  deleteOrder(){
    this.restService.deleteOrder(this.selectedOrderId).subscribe(_ => {
      this.restService.getOrdersOfMember(this.memberId).subscribe(result => {
        this.memberOrders = result;
      });
    });
  }

  addOrder(newOrder: AddOrderDto){
    this.restService.addOrder(newOrder).subscribe(result => {
      if(result.member.id === this.memberId){
        this.memberOrders.push(result);
      }
    });
  }

  openAddOrderModal(){
    this.isAddOrderVisible = true;
  }

  closeAddModal(){
    this.isAddOrderVisible = false;
  }

  selectedOrderChanged(orderId: number){
    this.selectedOrderId = orderId;
  }

  setFormValues() {
    this.editMemberForm.controls['title'].setValue(this.member.title);
    this.editMemberForm.controls['additionalTitle'].setValue(
      this.member.additionalTitle
    );
    this.editMemberForm.controls['firstName'].setValue(this.member.firstName);
    this.editMemberForm.controls['lastName'].setValue(this.member.lastName);
    this.editMemberForm.controls['plz'].setValue(this.member.plz);
    this.editMemberForm.controls['city'].setValue(this.member.city);
    this.editMemberForm.controls['street'].setValue(this.member.street);
    this.editMemberForm.controls['email'].setValue(this.member.email);
    this.editMemberForm.controls['contactTypeId'].setValue(
      this.contactTypes[
        this.contactTypes.findIndex((x) => x.id == this.member.contactType.id)
      ].id
    );
    this.editMemberForm.controls['deliveryAddress'].setValue(
      this.member.delAddress
    );
    this.editMemberForm.controls['privateNumber'].setValue(
      this.member.privateNumber
    );
    this.editMemberForm.controls['secondaryNumber'].setValue(
      this.member.secondaryNumber
    );
    this.editMemberForm.controls['internInfo'].setValue(this.member.internInfo);
    this.editMemberForm.controls['permanentInfo'].setValue(
      this.member.permanentInfo
    );
    this.editMemberForm.controls['active'].setValue(this.member.active);
    this.editMemberForm.controls['password'].setValue(this.member.password);
    this.editMemberForm.controls['webId'].setValue(this.member.webId);
  }

  get title() {
    return this.editMemberForm.controls['title'];
  }
  get additionalTitle() {
    return this.editMemberForm.controls['additionalTitle'];
  }
  get firstName() {
    return this.editMemberForm.controls['firstName'];
  }
  get lastName() {
    return this.editMemberForm.controls['lastName'];
  }
  get plz() {
    return this.editMemberForm.controls['plz'];
  }
  get city() {
    return this.editMemberForm.controls['city'];
  }
  get street() {
    return this.editMemberForm.controls['street'];
  }
  get email() {
    return this.editMemberForm.controls['email'];
  }
  get contactType() {
    return this.editMemberForm.controls['contactTypeId'];
  }
  get deliveryAddress() {
    return this.editMemberForm.controls['deliveryStreet'];
  }
  get privateNumber() {
    return this.editMemberForm.controls['privateNumber'];
  }
  get secondaryNumber() {
    return this.editMemberForm.controls['secondaryNumber'];
  }
  get internInfo() {
    return this.editMemberForm.controls['internInfo'];
  }
  get permanentInfo() {
    return this.editMemberForm.controls['permanentInfo'];
  }
  get active() {
    return this.editMemberForm.controls['active'];
  }
  get password() {
    return this.editMemberForm.controls['password'];
  }
}
