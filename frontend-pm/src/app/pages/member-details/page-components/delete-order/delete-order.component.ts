import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-delete-order',
  templateUrl: './delete-order.component.html',
  styleUrls: ['./delete-order.component.css']
})
export class DeleteOrderComponent implements OnInit {

  @Output() modalClosed = new EventEmitter();
  @Output() orderDeleted = new EventEmitter();
  @Input() isDeleteOrderVisible: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  closeModal(){
    this.modalClosed.emit();
  }

  deleteOrder(){
    this.orderDeleted.emit();
  }

}
