import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MemberDetailsRoutingModule } from './member-details-routing.module';
import { MemberDetailsPageComponent } from './member-details-page/member-details-page.component';
import { MemberDetailComponent } from './page-components/member-detail/member-detail.component';
import { MemberOrdersComponent } from './page-components/member-orders/member-orders.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddOrderComponent } from './page-components/add-order/add-order.component';
import { DeleteOrderComponent } from './page-components/delete-order/delete-order.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    MemberDetailsPageComponent,
    MemberDetailComponent,
    MemberOrdersComponent,
    AddOrderComponent,
    DeleteOrderComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MemberDetailsRoutingModule,
    SharedModule
  ]
})
export class MemberDetailsModule { }
