import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService } from 'src/app/core/rest.service';
import { SidebarService } from 'src/app/core/sidebar.service';
import { DetailedMemberDto } from 'src/app/models/DetailedMemberDto';

@Component({
  selector: 'app-member-details-page',
  templateUrl: './member-details-page.component.html',
  styleUrls: ['./member-details-page.component.css']
})
export class MemberDetailsPageComponent implements OnInit {

  isSidebarCollapsed = false;
  member: DetailedMemberDto = {} as DetailedMemberDto;
  memberId = -1;
  constructor(private sidebarService: SidebarService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.memberId = Number(this.activatedRoute.snapshot.paramMap.get('id'));  
    this.sidebarService.listen().subscribe(result => this.isSidebarCollapsed = result);
  }

}
