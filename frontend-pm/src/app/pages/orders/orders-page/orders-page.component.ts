import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/core/rest.service';
import { SidebarService } from 'src/app/core/sidebar.service';
import { AddOrderDto } from 'src/app/models/AddOrderDto';
import { ShortOrderDto } from 'src/app/models/ShortOrderDto';

@Component({
  selector: 'app-orders-page',
  templateUrl: './orders-page.component.html',
  styleUrls: ['./orders-page.component.css'],
})
export class OrderPageComponent implements OnInit {
  constructor(
    private restService: RestService,
    private sidebarService: SidebarService
  ) {}

  ngOnInit(): void {
    this.sidebarService
      .listen()
      .subscribe((result) => (this.isSidebarCollapsed = result));
    this.restService.getOrders().subscribe((result) => {
      this.orders = result.pageOrders;
      this.orderCount = result.orderCount;
    });
  }

  orders: ShortOrderDto[] = [];
  orderCount = 0;
  pageSize = 10;
  currentPage = 1;
  currentSearchString = '';
  isSidebarCollapsed = false;

  addOrder(newOrder: AddOrderDto) {
    this.restService.addOrder(newOrder).subscribe((_) => {
      this.restService.getOrders().subscribe((result) => {
        this.orders = result.pageOrders;
        this.orderCount = result.orderCount;
      });
    });
  }

  deleteOrder(orderId: number) {
    this.restService.deleteOrder(orderId).subscribe((_) => {
      this.orders = this.orders.filter((x) => x.id !== orderId);
    });
  }

  setPage(pageNr: number) {
    this.currentPage = pageNr;

    this.restService
      .getOrders(this.currentSearchString, this.currentPage, this.pageSize)
      .subscribe((result) => {
        this.orders = result.pageOrders;
        this.orderCount = result.orderCount;
      });
  }

  setSearchString(searchString: string) {
    this.currentSearchString = searchString;

    this.restService
      .getOrders(this.currentSearchString, this.currentPage, this.pageSize)
      .subscribe((result) => {
        this.orders = result.pageOrders;
        this.orderCount = result.orderCount;
      });
  }
}
