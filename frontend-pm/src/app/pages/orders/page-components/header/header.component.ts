import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  catchError,
  debounceTime,
  filter,
  map,
  mergeMap,
  Subject,
  switchMap,
  tap,
} from 'rxjs';
import { RestService } from 'src/app/core/rest.service';
import { ShortOrderDto } from 'src/app/models/ShortOrderDto';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor() {}

  searchFilterOptions = ['Einfache Suche'];
  searchString = '';
  orders: ShortOrderDto[] = [];
  searchSubject: Subject<any> = new Subject();

  @Output() searchStringChanged = new EventEmitter<string>();

  ngOnInit(): void {
    this.searchSubject
      .pipe(
        debounceTime(500)
      )
      .subscribe((result) => {
        this.searchStringChanged.emit(result);
      });
  }

  onKeyUp() {
    this.searchSubject.next(this.searchString);
  }
}
