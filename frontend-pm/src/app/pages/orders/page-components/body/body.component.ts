import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from 'src/app/core/rest.service';
import { AddOrderDto } from 'src/app/models/AddOrderDto';
import { ShortOrderDto } from 'src/app/models/ShortOrderDto';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css'],
})
export class BodyComponent implements OnInit {
  constructor(private router: Router) {}

  @Output() addOrder = new EventEmitter<AddOrderDto>();
  @Output() deleteOrder = new EventEmitter<number>();
  @Input() orders: ShortOrderDto[] = [];

  isAddOrderVisible = false;
  isDeleteOrderVisible = false;
  selectedOrderId = 0;

  ngOnInit(): void {
  }

  openDetailedOrderView(orderId: number) {
    this.router.navigateByUrl(`orders/order/${orderId}`);
  }

  openDeleteDialog() {
    this.isDeleteOrderVisible = true;
  }

  openAddDialog() {
    this.isAddOrderVisible = true;
  }

  closeAddDialog() {
    this.isAddOrderVisible = false;
  }

  closeDeleteDialog() {
    this.isDeleteOrderVisible = false;
  }

  addNewOrder(newOrder: AddOrderDto) {
    this.addOrder.emit(newOrder);
    this.isAddOrderVisible = false;
  }

  deleteOrderClicked(){
    this.deleteOrder.emit(this.selectedOrderId);
    this.isDeleteOrderVisible = false;
  }

  orderSelected(orderId: number) {
    this.selectedOrderId = orderId;
  }
}
