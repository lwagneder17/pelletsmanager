import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-delete-order',
  templateUrl: './delete-order.component.html',
  styleUrls: ['./delete-order.component.css'],
})
export class DeleteOrderComponent implements OnInit {
  constructor() {}

  @Output() deleteOrderClosed = new EventEmitter();
  @Output() deleteOrderClicked = new EventEmitter();
  @Input() deleteOrderVisible: boolean = false;

  ngOnInit(): void {}

  closeModal() {
    this.deleteOrderClosed.emit();
  }

  deleteOrder() {
    this.deleteOrderClicked.emit();
  }
}
