import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersPageRoutingModule } from './orders-page-routing.module';
import { HeaderComponent } from './page-components/header/header.component';
import { BodyComponent } from './page-components/body/body.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrderPageComponent } from './orders-page/orders-page.component';
import { DeleteOrderComponent } from './page-components/delete-order/delete-order.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    OrderPageComponent,
    HeaderComponent,
    BodyComponent,
    DeleteOrderComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    OrdersPageRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class OrdersPageModule { }
