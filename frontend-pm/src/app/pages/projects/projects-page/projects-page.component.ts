import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/core/rest.service';
import { SidebarService } from 'src/app/core/sidebar.service';
import { AddProjectDto } from 'src/app/models/AddProjectDto';
import { ShortProjectDto } from 'src/app/models/ShortProjectDto';

@Component({
  selector: 'app-projects-page',
  templateUrl: './projects-page.component.html',
  styleUrls: ['./projects-page.component.css'],
})
export class ProjectsPageComponent implements OnInit {
  projects: ShortProjectDto[] = [];
  projectCount: number = 0;
  isSidebarCollapsed = false;
  selectedProjectId = -1;
  currentPage: number = 1;
  pageSize: number = 10;

  constructor(
    private sidebarService: SidebarService,
    private restService: RestService
  ) {}

  ngOnInit(): void {
    this.sidebarService
      .listen()
      .subscribe(result => (this.isSidebarCollapsed = result));

    console.log(`ngOnInit: calling Projects`);
    this.restService.getProjects().subscribe(result => {
      this.projects = result.pageProjects;
      this.projectCount = result.projectCount;
    });
  }

  deleteProject() {
    this.restService.deleteProject(this.selectedProjectId).subscribe((_) => {
      this.restService.getProjects().subscribe(result => {
        this.projects = result.pageProjects;
        this.projectCount = result.projectCount;
      });
    });
  }

  addProject(newProject: AddProjectDto) {
    this.restService.addProject(newProject).subscribe(_ => {
      this.restService.getProjects().subscribe(result => {
        this.projects = result.pageProjects;
        this.projectCount = result.projectCount;
      });
    });
  }

  selectedProjectChanged(projectId: number) {
    this.selectedProjectId = projectId;
  }

  setPage(pageNr: number){
    this.currentPage = pageNr;

    console.log(`setPage to ${this.currentPage}: calling Projects`);
    this.restService.getProjects(this.pageSize, this.currentPage).subscribe((result) => {
      this.projects = result.pageProjects;
      this.projectCount = result.projectCount;
    });
  }
}
