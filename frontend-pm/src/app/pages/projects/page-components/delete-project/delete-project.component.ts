import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-delete-project',
  templateUrl: './delete-project.component.html',
  styleUrls: ['./delete-project.component.css']
})
export class DeleteProjectComponent implements OnInit {

  @Output() modalClosed = new EventEmitter();
  @Output() deleteProject = new EventEmitter();
  @Input() deleteProjectVisible = false;

  constructor() { }

  ngOnInit(): void {
  }

  closeModal(){
    this.modalClosed.emit();
  }

  deleteButtonClicked(){
    this.deleteProject.emit();
  }
}
