import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AddProjectDto } from 'src/app/models/AddProjectDto';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {

  addProjectForm: FormGroup = new FormGroup({});

  @Output() closeAddModal = new EventEmitter();
  @Output() addProject = new EventEmitter<AddProjectDto>();
  @Input() addProjectVisible = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.addProjectForm = this.formBuilder.group({
      shortDescription: new FormControl('', Validators.required),
      endOrderDate: new FormControl('', Validators.required)
    });
  }

  onSubmit(){
    this.addProject.emit(this.addProjectForm.value);
  }

  closeModal(){
    this.closeAddModal.emit();
  }

  get projectName() { return this.addProjectForm.controls['shortDescription']; }
  get endOrderDate() { return this.addProjectForm.controls['endOrderDate']; }
}
