import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AddProjectDto } from 'src/app/models/AddProjectDto';
import { ShortProjectDto } from 'src/app/models/ShortProjectDto';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  @Output() deleteProject = new EventEmitter();
  @Output() addProject = new EventEmitter<AddProjectDto>();
  @Output() selectionChanged = new EventEmitter<number>();
  @Input() projects: ShortProjectDto[] = [];

  isDeleteModalVisible = false;
  selectedProjectId = -1;
  isAddModalVisible = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  closeModal(){
    this.isDeleteModalVisible = false;
    this.isAddModalVisible = false;
  }

  deleteButtonClicked(){
    this.isDeleteModalVisible = false;
    this.deleteProject.emit();
  }

  selectedProjectChanged(projectId: number){
    if(this.selectedProjectId === projectId){
      this.selectedProjectId = -1;
    }
    else{
      this.selectedProjectId = projectId;
      this.selectionChanged.emit(projectId);
    }
  }

  openDeleteModal(){
    this.isDeleteModalVisible = true;
  }

  openAddModal(){
    this.isAddModalVisible = true;
  }

  addNewProject(newProject: AddProjectDto){
    this.isAddModalVisible = false;
    this.addProject.emit(newProject);
  }

  openProjectDetails(projectId: number){
    this.router.navigateByUrl(`projects/project/${projectId}`);
  }
}
