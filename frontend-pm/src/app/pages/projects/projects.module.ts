import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsPageComponent } from './projects-page/projects-page.component';
import { HeaderComponent } from './page-components/header/header.component';
import { BodyComponent } from './page-components/body/body.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddProjectComponent } from './page-components/add-project/add-project.component';
import { DeleteProjectComponent } from './page-components/delete-project/delete-project.component';


@NgModule({
  declarations: [
    ProjectsPageComponent,
    HeaderComponent,
    BodyComponent,
    AddProjectComponent,
    DeleteProjectComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ProjectsRoutingModule,
    SharedModule
  ]
})
export class ProjectsModule { }
