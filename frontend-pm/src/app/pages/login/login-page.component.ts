import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestService } from 'src/app/core/rest.service';
import { SidebarService } from 'src/app/core/sidebar.service';
import { LoginDto } from 'src/app/models/LoginDto';
import { UserDto } from 'src/app/models/UserDto';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
})
export class LoginPageComponent {
  constructor(
    private restService: RestService,
    private router: Router
  ) {}

  username: string = '';
  password: string = '';
  user: UserDto | null = null;
  isHidden: boolean = true;

  onLoginBtnClicked() {
    this.restService
      .getUserValidation({ password: this.password, username: this.username })
      .subscribe((x) => {
        this.user = x;

        if (this.user !== null) {
          localStorage.setItem('userId', this.user.id.toString());
          localStorage.setItem('userShortForm', this.user.shortForm);

          const url = sessionStorage.getItem('originalUrl');
          url === null ? this.router.navigate(['']) : this.router.navigate([url]);
          sessionStorage.removeItem('originalUrl');
        }
        else{
          this.isHidden = false;
        }
      });
  }
}
