import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {

  constructor() { }

  @Output() closeModalClicked = new EventEmitter();
  @Output() deleteButtonClicked = new EventEmitter();
  @Input() isDeleteUserVisible = false;

  ngOnInit(): void {
    console.log('ngOnInit of DeleteUser');
  }

  closeModal(){
    this.closeModalClicked.emit();
  }

  deleteUser(){
    this.deleteButtonClicked.emit();
  }

}
