import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EditUserDto } from 'src/app/models/EditUserDto';
import { UserDto } from 'src/app/models/UserDto';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnChanges {

  editUserForm: FormGroup = new FormGroup({});

  @Output() closeEditModalClicked = new EventEmitter();
  @Output() editButtonClicked = new EventEmitter<EditUserDto>();
  @Input() user: UserDto = {} as UserDto;

  constructor(formBuilder: FormBuilder) {
    this.editUserForm = formBuilder.group({
      active: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      shortForm: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.active.setValue(this.user.active);
    this.lastname.setValue(this.user.lastName);
    this.firstname.setValue(this.user.firstName);
    this.shortform.setValue(this.user.shortForm);
    this.password.setValue(this.user.password);
  }

  ngOnInit(): void {
  }

  onSubmit(){
    console.table(this.editUserForm.value);
    this.editButtonClicked.emit(this.editUserForm.value);
  }

  closeModal(){
    this.closeEditModalClicked.emit();
  }

  get active() { return this.editUserForm.controls['active']; }
  get lastname() { return this.editUserForm.controls['lastName']; }
  get firstname() { return this.editUserForm.controls['firstName']; }
  get shortform() { return this.editUserForm.controls['shortForm']; }
  get password() { return this.editUserForm.controls['password']; }
}
