import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AddUserDto } from 'src/app/models/AddUserDto';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) {
    this.addUserForm = this.formBuilder.group({
      lastName: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
   }

  @Output() closeAddModal = new EventEmitter();
  @Output() addButtonClicked = new EventEmitter<AddUserDto>();

  addUserForm: FormGroup = new FormGroup({});

  ngOnInit(): void {
    console.log('ngOnInit of AddUserComponent');
  }

  onSubmit(){
    console.table(this.addUserForm.value);
    this.addButtonClicked.emit(this.addUserForm.value);
  }

  closeModal(){
    this.closeAddModal.emit();
  }
  
  get lastname() { return this.addUserForm.controls['lastName']; }
  get firstname() { return this.addUserForm.controls['firstName']; }
  get password() { return this.addUserForm.controls['password']; }
}
