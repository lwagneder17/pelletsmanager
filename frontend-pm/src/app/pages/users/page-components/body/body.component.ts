import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AddUserDto } from 'src/app/models/AddUserDto';
import { EditUserDto } from 'src/app/models/EditUserDto';
import { UserDto } from 'src/app/models/UserDto';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  @Output() deleteUser = new EventEmitter<number>();
  @Output() addUser = new EventEmitter<AddUserDto>();
  @Output() editUser = new EventEmitter<EditUserDto>();
  @Output() setEditUserToEdit = new EventEmitter<UserDto>();
  @Input() users: UserDto[] = [];

  selectedUserId = -1;
  userToEdit: UserDto = {} as UserDto;
  isDeleteModalVisible = false;
  isAddModalVisible = false;
  isEditModalVisible = false;

  constructor() { }

  ngOnInit(): void {
  }

  userSelectionChanged(userId: number){
    if(this.selectedUserId === userId){
      this.selectedUserId = -1;
    }
    else{
      this.selectedUserId = userId;
    }
  }

  deleteButtonClicked(){
    this.deleteUser.emit(this.selectedUserId);
    this.isDeleteModalVisible = false;
    this.selectedUserId = -1;
  }

  addButtonClicked(newUser: AddUserDto){
    this.addUser.emit(newUser);
    this.isAddModalVisible = false;
  }

  editButtonClicked(editUser: EditUserDto){
    this.isEditModalVisible = false;
    this.editUser.emit(editUser);
  }

  openDeleteModal(){
    this.isDeleteModalVisible = true;
  }

  closeDeleteModal(){
    this.isDeleteModalVisible = false;
  }

  openAddModal(){
    this.isAddModalVisible = true;
  }

  closeAddModal(){
    this.isAddModalVisible = false;
  }

  closeEditModal(){
    this.isEditModalVisible = false;
  }

  openEditModal(user: UserDto){
    this.isEditModalVisible = true;
    this.userToEdit = user;
    this.setEditUserToEdit.emit(user);
  }
}
