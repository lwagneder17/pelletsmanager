import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/core/rest.service';
import { AddUserDto } from 'src/app/models/AddUserDto';
import { EditUserDto } from 'src/app/models/EditUserDto';
import { UserDto } from 'src/app/models/UserDto';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.css']
})
export class UsersPageComponent implements OnInit {

  constructor(private restService: RestService) { }

  isSidebarCollapsed = false;
  userCount = 0;
  pageSize = 10;
  currentPage = 1;
  users: UserDto[] = [];
  selectedUserId = -1;
  userToEdit: UserDto = {} as UserDto;

  ngOnInit(): void {
    this.restService.getUsers().subscribe(result => {
      this.users = result.pageUsers;
      this.userCount = result.userCount;
    });
  }

  setPage(pageNr: number){
    this.currentPage = pageNr;
  }

  deleteUser(userId: number){
    this.restService.deleteUser(userId, this.pageSize, this.currentPage).subscribe(result => {
      this.users = result.pageUsers;
      this.userCount = result.userCount;
    });
  }

  addUser(newUser: AddUserDto){
    this.restService.addUser(newUser, this.pageSize, this.currentPage).subscribe(result => {
      this.users = result.pageUsers;
      this.userCount = result.userCount;
    });
  }

  editUser(editUser: EditUserDto){
    this.restService.editUser(this.userToEdit.id, editUser).subscribe(result => {
      this.users[this.users.indexOf(this.userToEdit)] = result;
    });
  }

  setEditUserToEdit(user: UserDto){
    this.userToEdit = user;
  }

}
