import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { UsersPageComponent } from './users-page/users-page.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BodyComponent } from './page-components/body/body.component';
import { HeaderComponent } from './page-components/header/header.component';
import { AddUserComponent } from './page-components/add-user/add-user.component';
import { DeleteUserComponent } from './page-components/delete-user/delete-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditUserComponent } from './page-components/edit-user/edit-user.component';


@NgModule({
  declarations: [
    UsersPageComponent,
    BodyComponent,
    HeaderComponent,
    AddUserComponent,
    DeleteUserComponent,
    EditUserComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class UsersModule { }
