import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { RestService } from 'src/app/core/rest.service';
import { SidebarService } from 'src/app/core/sidebar.service';
import { ContactTypeDto } from 'src/app/models/ContactTypeDto';
import { DetailedOrderDto } from 'src/app/models/DetailedOrderDto';
import { EditOrderDto } from 'src/app/models/EditOrderDto';
import { OrderTypeDto } from 'src/app/models/OrderTypeDto';
import { ShortProjectSupplierDto } from 'src/app/models/ShortProjectSupplierDto';

@Component({
  selector: 'app-order-details-page',
  templateUrl: './order-details-page.component.html',
  styleUrls: ['./order-details-page.component.css'],
})
export class OrderDetailsPageComponent implements OnInit {
  isSidebarCollapsed = false;
  order: DetailedOrderDto | undefined = undefined;
  orderId = -1;
  editOrderForm: FormGroup = new FormGroup({});
  editOrder: EditOrderDto = {} as EditOrderDto;
  contactTypes: ContactTypeDto[] = [];
  orderTypes: OrderTypeDto[] = [];
  projectSuppliers: ShortProjectSupplierDto[] = [];
  selectedOrderId = -1;
  isAddOrderVisible = false;
  isDeleteOrderVisible = false;
  selectedSupplier: ShortProjectSupplierDto = {} as ShortProjectSupplierDto;
  availableDeliveryDateOptions: string[] = [];

  constructor(
    private restService: RestService,
    private sidebarService: SidebarService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.editOrderForm = this.formBuilder.group({
      memberId: new FormControl('', Validators.required),
      projectSupplierId: new FormControl('', Validators.required),
      orderTypeId: new FormControl('', Validators.required),
      orderDate: new FormControl('', Validators.required),
      amountOrdered: new FormControl('', Validators.required),
      shortInfo: new FormControl(''),
      contactTypeId: new FormControl('', Validators.required),
      wishedDelDate: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.orderId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.sidebarService.listen().subscribe((result) => {
      this.isSidebarCollapsed = result;
      console.log('isSidebarCollapsed: ' + result);
    });

    forkJoin(
      this.restService.getOrderById(this.orderId),
      this.restService.getContactTypes(),
      this.restService.getOrderTypes(),
      this.restService.getProjectSuppliers()
    ).subscribe(
      ([
        orderResult,
        contactTypesResult,
        orderTypesResult,
        projectSuppliersResult,
      ]) => {
        this.order = orderResult;
        this.availableDeliveryDateOptions =
          this.order?.availableDeliveryDateOptions;
        this.orderTypes = orderTypesResult;
        this.contactTypes = contactTypesResult;
        this.projectSuppliers = projectSuppliersResult;
        this.selectedSupplier = this.order?.projectSupplier;

        this.setFormValues();
      }
    );

    this.editOrderForm.controls['projectSupplierId'].valueChanges.subscribe(
      (selectedSupplierId) => {
        if(selectedSupplierId !== this.selectedSupplier){
          this.selectedSupplier =
          this.projectSuppliers[
            this.projectSuppliers.findIndex(
              (x) => x.id === Number(selectedSupplierId)
            )
          ];
        this.restService
          .getAvailableDeliveryDates(this.selectedSupplier.id)
          .subscribe((result) => (this.availableDeliveryDateOptions = result));
        }
      }
    );
  }

  onSubmit() {
    this.editOrder = this.editOrderForm.value;
    this.editOrder.userId = +localStorage.userId;
    console.table(this.editOrder);

    this.restService
      .editOrder(this.editOrder, this.orderId)
      .subscribe((result) => {
        this.order = result;
      });
  }

  resetForm() {
    this.setFormValues();
  }

  selectedOrderChanged(orderId: number) {
    this.selectedOrderId = orderId;
  }

  setFormValues() {
    this.editOrderForm.controls['memberId'].setValue(this.order?.member.id);
    this.editOrderForm.controls['projectSupplierId'].setValue(
      this.order?.projectSupplier.id
    );
    this.editOrderForm.controls['amountOrdered'].setValue(
      this.order?.amountOrdered
    );
    this.editOrderForm.controls['contactTypeId'].setValue(
      this.order?.contactType.id
    );
    this.editOrderForm.controls['orderDate'].setValue(this.order?.orderDate);
    this.editOrderForm.controls['orderTypeId'].setValue(
      this.order?.orderType.id
    );
    this.editOrderForm.controls['shortInfo'].setValue(this.order?.shortInfo);
    this.deliveryDateOption.setValue(0);
  }

  get member() {
    return this.editOrderForm.controls['memberId'];
  }
  get projectSupplier() {
    return this.editOrderForm.controls['projectSupplierId'];
  }
  get amountOrdered() {
    return this.editOrderForm.controls['amountOrdered'];
  }
  get contactType() {
    return this.editOrderForm.controls['contactTypeId'];
  }
  get orderDate() {
    return this.editOrderForm.controls['orderDate'];
  }
  get orderType() {
    return this.editOrderForm.controls['orderTypeId'];
  }
  get shortInfo() {
    return this.editOrderForm.controls['shortInfo'];
  }
  get deliveryDateOption() {
    return this.editOrderForm.controls['wishedDelDate'];
  }
}
