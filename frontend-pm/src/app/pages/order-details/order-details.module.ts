import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderDetailsPageComponent } from './order-details-page/order-details-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    OrderDetailsPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class OrderDetailsModule { }
