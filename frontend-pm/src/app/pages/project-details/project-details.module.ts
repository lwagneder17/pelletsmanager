import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectDetailsRoutingModule } from './project-details-routing.module';
import { ProjectDetailsPageComponent } from './project-details-page/project-details-page.component';
import { ProjectInformationsComponent } from './page-components/project-informations/project-informations.component';
import { ProjectSupplierDetailsComponent } from './page-components/project-supplier-details/project-supplier-details.component';
import { ProjectStatsComponent } from './page-components/project-stats/project-stats.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddProjectSupplierComponent } from './page-components/add-project-supplier/add-project-supplier.component';
import { EditProjectSupplierComponent } from './page-components/edit-project-supplier/edit-project-supplier.component';
import { DeleteProjectSupplierComponent } from './page-components/delete-project-supplier/delete-project-supplier.component';


@NgModule({
  declarations: [
    ProjectDetailsPageComponent,
    ProjectInformationsComponent,
    ProjectSupplierDetailsComponent,
    ProjectStatsComponent,
    AddProjectSupplierComponent,
    EditProjectSupplierComponent,
    DeleteProjectSupplierComponent
  ],
  imports: [
    CommonModule,
    ProjectDetailsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class ProjectDetailsModule { }
