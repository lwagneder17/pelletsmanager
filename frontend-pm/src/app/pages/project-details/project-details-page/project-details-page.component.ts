import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService } from 'src/app/core/rest.service';
import { SidebarService } from 'src/app/core/sidebar.service';
import { DetailedProjectDto } from 'src/app/models/DetailedProjectDto';
import { EditProjectDto } from 'src/app/models/EditProjectDto';

@Component({
  selector: 'app-project-details-page',
  templateUrl: './project-details-page.component.html',
  styleUrls: ['./project-details-page.component.css']
})
export class ProjectDetailsPageComponent implements OnInit {

  isSidebarCollapsed = false;
  isProjectActive = false;
  projectId = -1;
  project: DetailedProjectDto = {} as DetailedProjectDto;

  constructor(private sidebarService: SidebarService, private restService: RestService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.projectId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.sidebarService.listen().subscribe(result => this.isSidebarCollapsed = result);
    this.restService.getProject(this.projectId).subscribe(result => {this.project = result; this.isProjectActive = this.project.active});
  }

  updateProject(editProject: EditProjectDto){
    editProject.history = this.project.history;
    editProject.active = this.isProjectActive;
    this.restService.updateProject(editProject, this.projectId).subscribe(result => this.project = result);
  }
}
