import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RestService } from 'src/app/core/rest.service';
import { AddProjectSupplierDto } from 'src/app/models/AddProjectSupplierDto';
import { SupplierDto } from 'src/app/models/SupplierDto';

@Component({
  selector: 'app-add-project-supplier',
  templateUrl: './add-project-supplier.component.html',
  styleUrls: ['./add-project-supplier.component.css']
})
export class AddProjectSupplierComponent implements OnInit, OnChanges {

  suppliers: SupplierDto[] = [];
  addProjectSupplierForm: FormGroup = new FormGroup({});
  selectedSupplier: SupplierDto = {} as SupplierDto;
  newProjectSupplier: AddProjectSupplierDto = {} as AddProjectSupplierDto;

  @Input() isAddModalVisible = false;
  @Input() projectId = -1;
  @Output() addButtonClicked = new EventEmitter<AddProjectSupplierDto>();
  @Output() closeButtonClicked = new EventEmitter();


  constructor(private restService: RestService, formBuilder: FormBuilder) {
    this.addProjectSupplierForm = formBuilder.group({
      supplierId: new FormControl(''),
      pelletsPrice: new FormControl(''),
      alpPrice: new FormControl(''),
      endOrderDate: new FormControl(''),
      minOrderAmount: new FormControl('')
    });

    this.addProjectSupplierForm.controls['supplierId'].valueChanges.subscribe(x => {
      console.log(`selectedSupplierId changed to: ${x}`)
      this.selectedSupplier = this.suppliers[this.suppliers.findIndex(y => y.id == x)];
   })
  }
  ngOnChanges(changes: SimpleChanges): void {
    if(this.projectId !== -1){
      this.restService.getAvailableSuppliersForProject(this.projectId).subscribe(result => 
        {
          this.suppliers = result;
          this.supplier.setValue(this.suppliers[0].id);
        }
      );
    }
  }

  ngOnInit(): void {
  }

  closeModal(){
    this.closeButtonClicked.emit();
  }

  onSubmit(){
    this.newProjectSupplier = this.addProjectSupplierForm.value;
    this.addButtonClicked.emit(this.newProjectSupplier);
  }

  get supplier() {return this.addProjectSupplierForm.controls['supplierId']}
}
