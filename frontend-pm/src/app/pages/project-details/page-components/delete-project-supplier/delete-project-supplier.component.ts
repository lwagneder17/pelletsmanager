import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-delete-project-supplier',
  templateUrl: './delete-project-supplier.component.html',
  styleUrls: ['./delete-project-supplier.component.css']
})
export class DeleteProjectSupplierComponent implements OnInit {

  @Input() isDeleteProjectSupplierVisible = false;
  @Output() closeDeleteModal = new EventEmitter();
  @Output() deleteButtonClicked = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  closeModal(){
    this.closeDeleteModal.emit();
  }

  deleteProjectSupplier(){
    this.deleteButtonClicked.emit();
  }

}
