import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService } from 'src/app/core/rest.service';
import { AddProjectSupplierDto } from 'src/app/models/AddProjectSupplierDto';
import { DetailedProjectSupplierDto } from 'src/app/models/DetailedProjectSupplierDto';
import { EditProjectSupplierDto } from 'src/app/models/EditProjectSuppliersDto';
import { SupplierDto } from 'src/app/models/SupplierDto';

@Component({
  selector: 'app-project-supplier-details',
  templateUrl: './project-supplier-details.component.html',
  styleUrls: ['./project-supplier-details.component.css']
})
export class ProjectSupplierDetailsComponent implements OnInit, OnChanges {

  selectedProjectSupplierId = -1;
  selectedProjectSupplier: DetailedProjectSupplierDto = {} as DetailedProjectSupplierDto;
  editProjectSupplier: DetailedProjectSupplierDto = {} as DetailedProjectSupplierDto;
  isDeleteProjectSupplierVisible = false;
  isAddProjectSupplierVisible = false;
  isEditProjectSupplierVisible = false;
  projectId = -1;
  suppliers: SupplierDto[] = [];

  @Input() projectSuppliers: DetailedProjectSupplierDto[] = [];

  constructor(private restService: RestService, private activatedRoute: ActivatedRoute) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(!changes['projectSuppliers'].isFirstChange()){
      console.log(`projectSuppliers updated :${this.selectedProjectSupplierId}`);
      this.selectedProjectSupplier = this.projectSuppliers[0];
      this.selectedProjectSupplierId = this.selectedProjectSupplier.id;
    }
  }

  ngOnInit(): void {
    this.projectId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
  }

  projectSupplierSelected(projectSupplierId: number){
    console.log(`selected projectSupplierId: ${projectSupplierId}`);
    if(this.selectedProjectSupplierId !== projectSupplierId){
      this.selectedProjectSupplierId = projectSupplierId;
      this.selectedProjectSupplier = 
        this.projectSuppliers[this.projectSuppliers.findIndex(x => x.id === projectSupplierId)];
    }
  }

  closeDeleteModal(){
    this.isDeleteProjectSupplierVisible = false;
  }

  openDeleteModal(){
    this.isDeleteProjectSupplierVisible = true;
  }

  deleteProjectSupplier(){
    this.restService.deleteProjectSupplier(this.selectedProjectSupplierId).subscribe(_ => this.projectSuppliers = this.projectSuppliers.filter(x => x.id !== this.selectedProjectSupplierId));
    this.isDeleteProjectSupplierVisible = false;
  }

  openAddModal(){
    this.isAddProjectSupplierVisible = true;
  }

  closeAddModal(){
    this.isAddProjectSupplierVisible = false;
  }

  addProjectSupplier(newProjectSupplier: AddProjectSupplierDto){
    newProjectSupplier.projectId = this.projectId;
    this.restService.addProjectSupplier(newProjectSupplier).subscribe(result => {
      this.projectSuppliers.push(result);
      this.isAddProjectSupplierVisible = false;
    });
  }

  openEditModal(projectSupplierId: number){
    console.log('');
    this.editProjectSupplier = 
    this.projectSuppliers[this.projectSuppliers.findIndex(x => x.id === projectSupplierId)];
    this.isEditProjectSupplierVisible = true;
  }

  closeEditModal(){
    this.isEditProjectSupplierVisible = false;
  }

  updateProjectSupplier(editedProjectSupplier: EditProjectSupplierDto){
    this.restService.editProjectSupplier(this.editProjectSupplier.id, editedProjectSupplier).subscribe(result => {
      const index = this.projectSuppliers.indexOf(this.editProjectSupplier);
      this.projectSuppliers[index] = result;
      this.selectedProjectSupplier = this.projectSuppliers[index];
      this.isEditProjectSupplierVisible = false;
    });
  }
}
