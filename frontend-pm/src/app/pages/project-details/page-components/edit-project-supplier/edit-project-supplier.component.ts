import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RestService } from 'src/app/core/rest.service';
import { DetailedProjectSupplierDto } from 'src/app/models/DetailedProjectSupplierDto';
import { EditProjectSupplierDto } from 'src/app/models/EditProjectSuppliersDto';
import { SupplierDto } from 'src/app/models/SupplierDto';

@Component({
  selector: 'app-edit-project-supplier',
  templateUrl: './edit-project-supplier.component.html',
  styleUrls: ['./edit-project-supplier.component.css']
})
export class EditProjectSupplierComponent implements OnInit, OnChanges {

  editProjectSupplierForm: FormGroup = new FormGroup({});
  suppliers: SupplierDto[] = [];
  selectedSupplier: SupplierDto = {} as SupplierDto;
  editProjectSupplier: EditProjectSupplierDto = {} as EditProjectSupplierDto;

  @Input() isEditModalVisible = false;
  @Input() projectId = -1;
  @Input() projectSupplier: DetailedProjectSupplierDto = {} as DetailedProjectSupplierDto;
  @Output() closeEditModal = new EventEmitter();
  @Output() editButtonClicked = new EventEmitter<EditProjectSupplierDto>();

  constructor(private restService: RestService, formBuilder: FormBuilder) {
    this.editProjectSupplierForm = formBuilder.group({
      supplierId: new FormControl(''),
      pelletsPrice: new FormControl(''),
      alpPrice: new FormControl(''),
      endOrderDate: new FormControl(''),
      minOrderAmount: new FormControl('')
    });

    this.editProjectSupplierForm.controls['supplierId'].valueChanges.subscribe(x => {
      console.log(`selectedSupplierId changed to: ${x}`)
      this.selectedSupplier = this.suppliers[this.suppliers.findIndex(y => y.id == x)];
   })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.projectId !== -1){
      this.restService.getSuppliers(Number.MAX_SAFE_INTEGER).subscribe(result => 
        {
          this.suppliers = result.pageSuppliers;
          this.supplier.setValue(this.projectSupplier.supplierId);
          this.pelletsPrice.setValue(this.projectSupplier.pelletsPrice);
          this.alpPrice.setValue(this.projectSupplier.alpPrice);
          this.endOrderDate.setValue(this.projectSupplier.deliveryEnd);
          this.minOrderAmount.setValue(this.projectSupplier.minDeliveryAmount);
        }
      );
    }
  }

  ngOnInit(): void {
  }

  onSubmit(){
    this.editProjectSupplier = this.editProjectSupplierForm.value;
    this.editProjectSupplier.projectId = this.projectId;
    this.editButtonClicked.emit(this.editProjectSupplier);
  }

  closeModal(){
    this.closeEditModal.emit();
  }

  get supplier() {return this.editProjectSupplierForm.controls['supplierId']}
  get pelletsPrice() {return this.editProjectSupplierForm.controls['pelletsPrice']}
  get alpPrice() {return this.editProjectSupplierForm.controls['alpPrice']}
  get endOrderDate() {return this.editProjectSupplierForm.controls['endOrderDate']}
  get minOrderAmount() {return this.editProjectSupplierForm.controls['minOrderAmount']}
}
