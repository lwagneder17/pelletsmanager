import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService } from 'src/app/core/rest.service';
import { ContactTypeStatsDto } from 'src/app/models/ContactTypeStatsDto';
import { DetailedProjectDto } from 'src/app/models/DetailedProjectDto';
import { OrderTypeStatsDto } from 'src/app/models/OrderTypeStatsDto';
import { ProjectSupplierStatsDto } from 'src/app/models/ProjectSupplierStatsDto';

@Component({
  selector: 'app-project-stats',
  templateUrl: './project-stats.component.html',
  styleUrls: ['./project-stats.component.css']
})
export class ProjectStatsComponent implements OnInit {

  projectId = 0;
  selectedTabType = 'suppliers';
  supplierStats: ProjectSupplierStatsDto[] = [];
  orderTypeStats: OrderTypeStatsDto[] = [];
  contactTypeStats: ContactTypeStatsDto[] = [];

  @Input() project: DetailedProjectDto = {} as DetailedProjectDto;

  constructor(private restService: RestService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.projectId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.getStats();
  }

  tabClicked(type: string){
    if(this.selectedTabType !== type){
      this.selectedTabType = type;
      this.getStats();
    }
    console.log(`selectedTabType: ${this.selectedTabType}`);
  }

  getStats(){
    if(this.selectedTabType === 'suppliers' && this.supplierStats.length === 0){
      this.restService.getProjectSupplierStats(this.projectId).subscribe(result => {this.supplierStats = result; console.table(this.supplierStats)});
    }
    if(this.selectedTabType === 'orderType' && this.orderTypeStats.length === 0){
      this.restService.getOrderTypeStats(this.projectId).subscribe(result => {this.orderTypeStats = result; console.table(this.orderTypeStats)});
    }
    if(this.selectedTabType === 'contactType' && this.contactTypeStats.length === 0){
      this.restService.getContactTypeStats(this.projectId).subscribe(result => {this.contactTypeStats = result; console.table(this.contactTypeStats)});
    }
  }
}
