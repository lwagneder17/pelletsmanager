import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DetailedProjectDto } from 'src/app/models/DetailedProjectDto';
import { EditProjectDto } from 'src/app/models/EditProjectDto';

@Component({
  selector: 'app-project-informations',
  templateUrl: './project-informations.component.html',
  styleUrls: ['./project-informations.component.css']
})
export class ProjectInformationsComponent implements OnInit, OnChanges {

  projectInformationForm: FormGroup = new FormGroup({});

  @Input() project: DetailedProjectDto = {} as DetailedProjectDto;
  @Output() updateProjectClicked = new EventEmitter<EditProjectDto>();

  constructor(private formBuilder: FormBuilder) {
    this.projectInformationForm = this.formBuilder.group({
      shortDescription: new FormControl('', Validators.required),
      endOrderDate: new FormControl('', Validators.required),
      path: new FormControl('', Validators.required),
      expenses: new FormControl(''),
      loginPrefix: new FormControl(''),
      quantityPreviousYear: new FormControl(''),
      quantityOrdersPreviousYear: new FormControl(''),
      quantityOrdersPreviousYear0: new FormControl(''),
      ppiPrice: new FormControl(''),
      ppiValue: new FormControl(''),
      ppiDate: new FormControl(''),
      history: new FormControl('')
    });
   }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.shortDescription.setValue(this.project.shortDescription);
    this.endOrderDate.setValue(this.project.endOrderDate);
    this.path.setValue(this.project.path);
    this.expenses.setValue(this.project.expenses);
    this.loginPrefix.setValue(this.project.loginPrefix);
    this.quantityPreviousYear.setValue(this.project.quantityPreviousYear);
    this.quantityOrdersPreviousYear.setValue(this.project.quantityOrdersPreviousYear);
    this.quantityOrdersPreviousYear0.setValue(this.project.quantityOrdersPreviousYear0);
    this.ppiPrice.setValue(this.project.ppiPrice);
    this.ppiValue.setValue(this.project.ppiValue);
    this.ppiDate.setValue(this.project.ppiDate);
  }

  onSubmit(){
    this.updateProjectClicked.emit(this.projectInformationForm.value);
  }

  get shortDescription() {return this.projectInformationForm.controls['shortDescription']}
  get endOrderDate() {return this.projectInformationForm.controls['endOrderDate']}
  get path() {return this.projectInformationForm.controls['path']}
  get expenses() {return this.projectInformationForm.controls['expenses']}
  get loginPrefix() {return this.projectInformationForm.controls['loginPrefix']}
  get quantityPreviousYear() {return this.projectInformationForm.controls['quantityPreviousYear']}
  get quantityOrdersPreviousYear() {return this.projectInformationForm.controls['quantityOrdersPreviousYear']}
  get quantityOrdersPreviousYear0() {return this.projectInformationForm.controls['quantityOrdersPreviousYear0']}
  get ppiPrice() {return this.projectInformationForm.controls['ppiPrice']}
  get ppiValue() {return this.projectInformationForm.controls['ppiValue']}
  get ppiDate() {return this.projectInformationForm.controls['ppiDate']}

}
