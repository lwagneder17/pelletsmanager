import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService } from 'src/app/core/rest.service';
import { SidebarService } from 'src/app/core/sidebar.service';
import { SupplierDto } from 'src/app/models/SupplierDto';

@Component({
  selector: 'app-supplier-details-page',
  templateUrl: './supplier-details-page.component.html',
  styleUrls: ['./supplier-details-page.component.css']
})
export class SupplierDetailsPageComponent implements OnInit {

  isSidebarCollapsed = false;
  supplier: SupplierDto = {} as SupplierDto;
  supplierId = -1;
  constructor(private restService: RestService, private sidebarService: SidebarService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.supplierId = Number(this.activatedRoute.snapshot.paramMap.get('id'));  
    this.sidebarService.listen().subscribe(result => this.isSidebarCollapsed = result);
  }
}
