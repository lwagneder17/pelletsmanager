import {
  Component,
  Input,
  OnInit,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { RestService } from 'src/app/core/rest.service';
import { SupplierDto } from 'src/app/models/SupplierDto';

@Component({
  selector: 'app-supplier-detail',
  templateUrl: './supplier-detail.component.html',
  styleUrls: ['./supplier-detail.component.css'],
})
export class SupplierDetailComponent implements OnInit {
  @Input() supplierId: number = -1;

  supplier: SupplierDto = {} as SupplierDto;
  editSupplierForm: FormGroup = new FormGroup({});
  editSupplier: SupplierDto = {} as SupplierDto;
  selectedSupplierId = -1;
  isAddSupplierVisible = false;
  isDeleteSupplierVisible = false;

  constructor(
    private formBuilder: FormBuilder,
    private restService: RestService
  ) {
    this.editSupplierForm = this.formBuilder.group({
      id: new FormControl('', Validators.required),
      shortForm: new FormControl('', Validators.required),
      companyName: new FormControl('', Validators.required),
      active: new FormControl('', Validators.required),
      plz: new FormControl('', [
        Validators.required,
        Validators.maxLength(5),
        Validators.minLength(4),
      ]),
      city: new FormControl('', Validators.required),
      street: new FormControl('', Validators.required),
      greetingShort: new FormControl('', Validators.required),
      telephone: new FormControl(''),
      email: new FormControl(''),
      web: new FormControl(''),
      history: new FormControl(''),
      contactName: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.restService.getSupplierById(this.supplierId).subscribe((supplierResult) => {
          this.supplier = supplierResult;
          this.setFormValues();
        });
  }

  onSubmit() {
    this.editSupplier = this.editSupplierForm.value;

    this.restService
      .editSupplier(this.editSupplier, this.supplierId)
      .subscribe((result) => {
        this.supplier = result;
        this.setFormValues();
      });
  }

  resetForm() {
    this.setFormValues();
  }

  selectedSupplierChanged(supplierId: number) {
    this.selectedSupplierId = supplierId;
  }

  setFormValues() {
    this.editSupplierForm.controls['id'].setValue(this.supplier.id);
    this.editSupplierForm.controls['shortForm'].setValue(
      this.supplier.shortForm
    );
    this.editSupplierForm.controls['companyName'].setValue(
      this.supplier.companyName
    );
    this.editSupplierForm.controls['active'].setValue(this.supplier.active);
    this.editSupplierForm.controls['plz'].setValue(this.supplier.plz);
    this.editSupplierForm.controls['city'].setValue(this.supplier.city);
    this.editSupplierForm.controls['street'].setValue(this.supplier.street);
    this.editSupplierForm.controls['telephone'].setValue(
      this.supplier.telephone
    );
    this.editSupplierForm.controls['web'].setValue(this.supplier.web);
    this.editSupplierForm.controls['history'].setValue(this.supplier.history);
    this.editSupplierForm.controls['greetingShort'].setValue(
      this.supplier.greetingShort
    );
    this.editSupplierForm.controls['email'].setValue(this.supplier.email);
    this.editSupplierForm.controls['contactName'].setValue(
      this.supplier.contactName
    );
  }

  get id() {
    return this.editSupplierForm.controls['id'];
  }
  get shortForm() {
    return this.editSupplierForm.controls['shortForm'];
  }
  get companyName() {
    return this.editSupplierForm.controls['companyName'];
  }
  get active() {
    return this.editSupplierForm.controls['active'];
  }
  get plz() {
    return this.editSupplierForm.controls['plz'];
  }
  get street() {
    return this.editSupplierForm.controls['street'];
  }
  get city() {
    return this.editSupplierForm.controls['city'];
  }
  get greetingShort() {
    return this.editSupplierForm.controls['greetingShort'];
  }
  get contactName() {
    return this.editSupplierForm.controls['contactName'];
  }
}
