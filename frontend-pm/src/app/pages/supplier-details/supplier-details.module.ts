import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupplierDetailsPageComponent } from './supplier-details-page/supplier-details-page.component';
import { SupplierDetailComponent } from './page-components/supplier-detail/supplier-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    SupplierDetailsPageComponent,
    SupplierDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class SupplierDetailsModule { }
