import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupplierPageRoutingModule } from './supplier-page-routing.module';
import { SupplierPageComponent } from './supplier-page/supplier-page.component';
import { BodyComponent } from './page-components/body/body.component';
import { HeaderComponent } from './page-components/header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddSupplierComponent } from './page-components/add-supplier/add-supplier.component';
import { DeleteSupplierComponent } from './page-components/delete-supplier/delete-supplier.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    SupplierPageComponent,
    BodyComponent,
    HeaderComponent,
    AddSupplierComponent,
    DeleteSupplierComponent
  ],
  imports: [
    CommonModule,
    SupplierPageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    BodyComponent,
    HeaderComponent
  ]
})
export class SupplierPageModule { }
