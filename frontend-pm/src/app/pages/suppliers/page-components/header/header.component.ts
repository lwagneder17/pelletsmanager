import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { debounceTime, map, Subject, switchMap } from 'rxjs';
import { RestService } from 'src/app/core/rest.service';
import { SupplierDto } from 'src/app/models/SupplierDto';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private restService: RestService) { }

  @Output() suppliersChanged = new EventEmitter<SupplierDto[]>();

  searchString = "";
  filterOptions: string[] = [];
  selectedFilterOption: number = 0;
  suppliers: SupplierDto[] = [];
  searchSubject: Subject<any> = new Subject();


  ngOnInit(): void {
    console.log('sending request to: http://localhost:8080/pelletsBackend/suppliers/filterOptions');
    this.restService.getSupplierFilterOptions().subscribe(result => this.filterOptions = result);

    this.searchSubject.pipe(
      debounceTime(250),
      map(x => {
            console.log(`send request to: http://localhost:8080/suppliers/search/${x}`);
            return `/suppliers/search/${x}`;
          }),
      switchMap(x => 
        this.restService.getSuppliersBySearch(x))
    )
    .subscribe(result => { this.suppliersChanged.emit(result); });
  }

  onKeyUp(){
      this.searchSubject.next(this.searchString);
  }

  onFilterChanged(){
    console.log(`selected index: ${this.selectedFilterOption}`);
    this.restService.getSuppliersByFilter(this.selectedFilterOption).subscribe(result => {this.suppliersChanged.emit(result);});
  }
}