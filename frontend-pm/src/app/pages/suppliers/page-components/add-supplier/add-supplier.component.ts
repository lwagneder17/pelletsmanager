import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RestService } from 'src/app/core/rest.service';
import { AddSupplierDto } from 'src/app/models/AddSupplierDto';
import { ContactTypeDto } from 'src/app/models/ContactTypeDto';
import { SupplierDto } from 'src/app/models/SupplierDto';

@Component({
  selector: 'app-add-supplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.css']
})
export class AddSupplierComponent implements OnInit, OnChanges {

  contactTypes: ContactTypeDto[] = [];
  submitted: boolean = false;

  addSupplierForm: FormGroup = new FormGroup({});
  newSupplier: AddSupplierDto = {} as AddSupplierDto;

  constructor(private formBuilder: FormBuilder, private restService: RestService) { }

  @Output() addSupplierClosed = new EventEmitter<boolean>();
  @Output() newSupplierCreated = new EventEmitter<SupplierDto>();
  @Input() addSupplierVisible: boolean | undefined;

  ngOnInit(): void {
    this.addSupplierForm = this.formBuilder.group({
      companyName: new FormControl('', Validators.required),
      shortForm: new FormControl('', Validators.required),
      contactName: new FormControl(''),
      greetingShort: new FormControl('', Validators.required),
      email: new FormControl('', Validators.email),
      plz: new FormControl('', [Validators.required, Validators.maxLength(5), Validators.minLength(4)]),
      city: new FormControl('', Validators.required),
      street: new FormControl('', Validators.required),
      telephone: new FormControl(''),
      web: new FormControl(''),
      history: new FormControl(''),
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    
  }

  closeModal(){
    this.addSupplierForm.reset();
    this.addSupplierClosed.emit(true);
  }

  onSubmit(){
    this.newSupplier = this.addSupplierForm.value;

    if (this.addSupplierForm.value['shortForm'] === "Herr") {
      this.newSupplier.greetingLong = "Sehr geehrter Herr "
    }
    else if (this.addSupplierForm.value['shortForm'] === "Frau") {
      this.newSupplier.greetingLong = "Sehr geehrte Frau "
    }

    this.restService.addSupplier(this.newSupplier).subscribe(result => {this.newSupplierCreated.emit(result); this.addSupplierClosed.emit(true);});
  }

  get shortForm() { return this.addSupplierForm.controls['shortForm'];}

  get companyName() { return this.addSupplierForm.controls['companyName'];}

  get greetingShort() { return this.addSupplierForm.controls['greetingShort'];}

  get plz() { return this.addSupplierForm.controls['plz'];}

  get city() { return this.addSupplierForm.controls['city'];}

  get street() { return this.addSupplierForm.controls['street'];}
  
  get email() { return this.addSupplierForm.controls['email'];}
}
