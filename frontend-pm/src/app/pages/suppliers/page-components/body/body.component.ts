import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { SupplierDto } from 'src/app/models/SupplierDto';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  constructor(private router: Router) { }

  @Output() deleteSupplier = new EventEmitter<number>();
  @Output() addSupplier = new EventEmitter();
  @Input() suppliers: SupplierDto[] = [];

  isAddSupplierVisible = false;
  isDeleteSupplierVisible = false;
  selectedSupplierId = 0;

  ngOnInit(): void {
  }

  openDeleteDialog(){
    this.isDeleteSupplierVisible = true; 
  }
  
  openAddDialog(){
    this.isAddSupplierVisible = true;
  }

  openDetailedSupplierView(supplierId: number){
    this.router.navigateByUrl(`suppliers/supplier/${supplierId}`);
  }

  closeAddDialog(){
    this.isAddSupplierVisible = false;
  }

  closeDeleteDialog(){
    this.isDeleteSupplierVisible = false;
  }

  deleteSupplierClicked(){
    this.deleteSupplier.emit(this.selectedSupplierId);
  }

  addNewSupplier(newSupplier: SupplierDto){
    // this.suppliers.push(newSupplier);
    // this.suppliersChanged.emit(this.suppliers);
  }

  supplierSelected(supplierId: number){
    if(this.selectedSupplierId === supplierId){
      this.selectedSupplierId = 0;
    }
    else{
      this.selectedSupplierId = supplierId;
    }
  }
}
