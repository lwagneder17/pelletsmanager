import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RestService } from 'src/app/core/rest.service';

@Component({
  selector: 'app-delete-supplier',
  templateUrl: './delete-supplier.component.html',
  styleUrls: ['./delete-supplier.component.css']
})
export class DeleteSupplierComponent implements OnInit {

  constructor() { }

  @Output() deleteSupplierClosed = new EventEmitter();
  @Output() deleteSupplierClicked = new EventEmitter();
  @Input() deleteSupplierVisible: boolean = false;

  ngOnInit(): void {
  }

  closeModal(){
    this.deleteSupplierClosed.emit();
  }

  deleteSupplier(){
    this.deleteSupplierClicked.emit();
  }
}
