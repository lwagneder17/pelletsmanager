import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/core/rest.service';
import { SidebarService } from 'src/app/core/sidebar.service';
import { SupplierDto } from 'src/app/models/SupplierDto';

@Component({
  selector: 'app-supplier-page',
  templateUrl: './supplier-page.component.html',
  styleUrls: ['./supplier-page.component.css']
})
export class SupplierPageComponent implements OnInit {
  constructor(private restService: RestService, private sidebarService: SidebarService) { }

  suppliers: SupplierDto[] = [];
  supplierCount = 0;
  pageSize = 10;
  currentPage = 1;
  isAddSupplierVisible = false;
  isSidebarCollapsed = false;

  ngOnInit(): void {
    this.sidebarService.listen().subscribe(result => this.isSidebarCollapsed = result);

    console.log('ngOnInit: calling suppliers...');
    this.restService.getSuppliers(this.pageSize, this.currentPage).subscribe(result => {
      console.table(result);
      this.suppliers = result.pageSuppliers;
      this.supplierCount = result.supplierCount;
    });
  }

  deleteSupplier(supplierId: number){
    this.restService.deleteSupplier(supplierId).subscribe(_ => {
      this.restService.getSuppliers(this.pageSize, this.currentPage).subscribe(result => {
        this.suppliers = result.pageSuppliers;
        this.supplierCount = result.supplierCount;
      });
    })
  }

  setPage(pageNr: number){

  }
}
