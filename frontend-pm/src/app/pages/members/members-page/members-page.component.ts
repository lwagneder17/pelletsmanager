import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/core/rest.service';
import { SidebarService } from 'src/app/core/sidebar.service';
import { AddMemberDto } from 'src/app/models/AddMemberDto';
import { ShortMemberDto } from 'src/app/models/ShortMemberDto';

@Component({
  selector: 'app-member-page',
  templateUrl: './members-page.component.html',
  styleUrls: ['./members-page.component.css'],
})
export class MembersPageComponent implements OnInit {
  members: ShortMemberDto[] = [];
  memberCount = 0;
  pageSize = 10;
  isAddMemberVisible = false;
  isSidebarCollapsed = false;
  selectedSearchFilterId = 0;
  selectedMemberId = 0;
  currentPage = 1;
  selectedFilterId = 0;
  currentSearchString = '';

  constructor(
    private sidebarService: SidebarService,
    private restService: RestService
  ) {}

  ngOnInit(): void {
    this.sidebarService
      .listen()
      .subscribe((result) => (this.isSidebarCollapsed = result));

    console.log(
      `sending request to: http://localhost:8080/pelletsBackend/members`
    );
    this.restService.getMembers().subscribe((result) => {
      this.members = result.pageMembers;
      this.memberCount = result.memberCount;
    });
  }

  deleteMember() {
    this.restService.deleteMember(this.selectedMemberId).subscribe((_) => {
      this.members = this.members.filter((x) => x.id !== this.selectedMemberId);
      this.memberCount = this.members.length;
    });
  }

  addMember(newMember: AddMemberDto) {
    this.restService.addMember(newMember).subscribe((_) => {
      this.restService
        .getMembers(
          this.pageSize,
          this.currentPage,
          this.selectedFilterId,
          this.selectedSearchFilterId,
          this.currentSearchString
        )
        .subscribe((result) => {
          this.members = result.pageMembers;
          this.memberCount = result.memberCount;
        });
    });
  }

  getSearchResults(searchString: string) {
    this.currentSearchString = searchString;
    this.currentPage = 1;

    this.restService
      .getMembers(
        this.pageSize,
        this.currentPage,
        this.selectedFilterId,
        this.selectedSearchFilterId,
        this.currentSearchString
      )
      .subscribe((result) => {
        this.members = result.pageMembers;
        this.memberCount = result.memberCount;
      });
  }

  getFilterResults(filterOptionId: number) {
    this.selectedFilterId = filterOptionId;
    this.currentPage = 1;

    this.restService
      .getMembers(
        this.pageSize,
        this.currentPage,
        this.selectedFilterId,
        this.selectedSearchFilterId,
        this.currentSearchString
      )
      .subscribe((result) => {
        this.members = result.pageMembers;
        this.memberCount = result.memberCount;
      });
  }

  setSearchFilterId(searchFilterId: number) {
    this.currentSearchString = '';
    this.selectedSearchFilterId = searchFilterId;
  }

  setSelectedMemberId(memberId: number) {
    this.selectedMemberId = memberId;
  }

  setCurrentPage(pageNr: number) {
    this.currentPage = pageNr;

    this.restService
      .getMembers(
        this.pageSize,
        this.currentPage,
        this.selectedFilterId,
        this.selectedSearchFilterId,
        this.currentSearchString
      )
      .subscribe((result) => {
        this.members = result.pageMembers;
        this.memberCount = result.memberCount;
      });
  }
}
