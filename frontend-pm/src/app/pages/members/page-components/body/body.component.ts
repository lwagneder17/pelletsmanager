import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from 'src/app/core/rest.service';
import { AddMemberDto } from 'src/app/models/AddMemberDto';
import { ShortMemberDto } from 'src/app/models/ShortMemberDto';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  constructor(private router: Router) { }

  @Output() addMember = new EventEmitter<AddMemberDto>();
  @Output() deleteMember = new EventEmitter();
  @Output() selectedMemberChanged = new EventEmitter();
  @Input() members: ShortMemberDto[] = [];

  isAddMemberVisible = false;
  isDeleteMemberVisible = false;
  selectedMemberId = -1;

  ngOnInit(): void {
  }

  openDetailedMemberView(memberId: number){
    this.router.navigateByUrl(`members/member/${memberId}`);
  }

  openDeleteDialog(){
    this.isDeleteMemberVisible = true; 
  }
  
  openAddDialog(){
    this.isAddMemberVisible = true;
  }

  closeAddDialog(){
    this.isAddMemberVisible = false;
  }

  closeDeleteDialog(){
    this.isDeleteMemberVisible = false;
  }

  addNewMember(newMember: AddMemberDto){
    this.isAddMemberVisible = false;
    this.addMember.emit(newMember);
  }

  deleteMemberClicked(){
    this.isDeleteMemberVisible = false;
    this.deleteMember.emit();
  }

  memberSelected(memberId: number){
    if(this.selectedMemberId === memberId){
      this.selectedMemberId = -1;
    }
    else{
      this.selectedMemberId = memberId;
      this.selectedMemberChanged.emit(this.selectedMemberId);
    }
  }
}
