import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RestService } from 'src/app/core/rest.service';
import { AddMemberDto } from 'src/app/models/AddMemberDto';
import { ContactTypeDto } from 'src/app/models/ContactTypeDto';

@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.css']
})
export class AddMemberComponent implements OnInit, OnChanges {

  contactTypes: ContactTypeDto[] = [];
  submitted: boolean = false;

  addMemberForm: FormGroup = new FormGroup({});
  newMember: AddMemberDto = {} as AddMemberDto;

  constructor(private formBuilder: FormBuilder, private restService: RestService) { }

  @Output() addMemberClosed = new EventEmitter();
  @Output() addMemberClicked = new EventEmitter<AddMemberDto>();
  @Input() addMemberVisible: boolean = false;

  ngOnInit(): void {
    this.addMemberForm = this.formBuilder.group({
      title: new FormControl(''),
      additionalTitle: new FormControl(''),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      contactType: new FormControl(this.contactTypes[1], Validators.required),
      plz: new FormControl('', [Validators.required, Validators.maxLength(5), Validators.minLength(4)]),
      city: new FormControl('', Validators.required),
      street: new FormControl('', Validators.required),
      deliveryStreet: new FormControl(''),
      privateNumber: new FormControl('', Validators.required),
      secondaryNumber: new FormControl(''),
      internInfo: new FormControl(''),
      permanentInfo: new FormControl('')
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['addMemberVisible'].currentValue === true && this.contactTypes.length === 0){
      this.restService.getContactTypes().subscribe(result => this.contactTypes = result);
    }
  }

  closeModal(){
    this.addMemberForm.reset();
    this.addMemberClosed.emit();
  }

  onSubmit(){
    const contactTypeId = this.addMemberForm.value['contactType'];
    delete this.addMemberForm.value['contactType'];

    this.newMember = this.addMemberForm.value;
    this.newMember.contactTypeId = contactTypeId;
    this.addMemberClicked.emit(this.newMember);
  }

  get firstName() { return this.addMemberForm.controls['firstName']; }

  get lastName() { return this.addMemberForm.controls['lastName']; }

  get email() { return this.addMemberForm.controls['email']; }

  get contactType() { return this.addMemberForm.controls['contactType']; }

  get plz() { return this.addMemberForm.controls['plz']; }

  get city() { return this.addMemberForm.controls['city']; }

  get street() { return this.addMemberForm.controls['street']; }

  get privateNumber() { return this.addMemberForm.controls['privateNumber']; }

}
