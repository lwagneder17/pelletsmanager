import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { debounceTime, map, Subject, switchMap } from 'rxjs';
import { RestService } from 'src/app/core/rest.service';
import { ShortMemberDto } from 'src/app/models/ShortMemberDto';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private restService: RestService) { }

  @Output() selectedFilterOptionChanged = new EventEmitter<number>();
  @Output() selectedSearchFilterChanged = new EventEmitter<number>();
  @Output() searchStringChanged = new EventEmitter<string>();

  searchFilterOptions = ["Einfache Suche", "Namenssuche"]
  selectedSearchFilterId: number = 0;
  searchString = "";

  filterOptions: string[] = [];
  selectedFilterOption = 0;

  searchSubject: Subject<any> = new Subject();


  ngOnInit(): void {
    console.log('sending request to: http://localhost:8080/pelletsBackend/members/filterOptions');
    this.restService.getMemberFilterOptions().subscribe(result => this.filterOptions = result);

    this.searchSubject.pipe(
      debounceTime(250)
    )
    .subscribe(result => { this.searchStringChanged.emit(result); });
  }

  onKeyUp(){
      this.searchSubject.next(this.searchString);
  }

  onFilterChanged(){
    this.selectedFilterOptionChanged.emit(this.selectedFilterOption);
    if(this.searchString !== ""){
      this.searchString = "";
      this.searchStringChanged.emit(this.searchString);
    }
  }

  searchFilterChanged(){
    this.searchString = "";
    this.selectedSearchFilterChanged.emit(this.selectedSearchFilterId);
  }
}