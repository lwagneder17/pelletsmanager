import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RestService } from 'src/app/core/rest.service';

@Component({
  selector: 'app-delete-member',
  templateUrl: './delete-member.component.html',
  styleUrls: ['./delete-member.component.css']
})
export class DeleteMemberComponent implements OnInit {

  constructor(private restService: RestService) { }

  @Output() deleteMemberClosed = new EventEmitter();
  @Output() deleteMemberClicked = new EventEmitter();
  @Input() deleteMemberVisible: boolean = false;

  ngOnInit(): void {
  }

  closeModal(){
    this.deleteMemberClosed.emit();
  }

  deleteMember(){
    this.deleteMemberClicked.emit();
  }

}
