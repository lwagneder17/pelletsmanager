import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MembersRoutingModule } from './members-routing.module';
import { MembersPageComponent } from './members-page/members-page.component';
import { HeaderComponent } from './page-components/header/header.component';
import { BodyComponent } from './page-components/body/body.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddMemberComponent } from './page-components/add-member/add-member.component';
import { DeleteMemberComponent } from './page-components/delete-member/delete-member.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    MembersPageComponent,
    HeaderComponent,
    BodyComponent,
    AddMemberComponent,
    DeleteMemberComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MembersRoutingModule,
    SharedModule
  ]
})
export class MembersModule { }
