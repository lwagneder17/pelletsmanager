import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MembersModule } from './pages/members/members.module';
import { CoreModule } from './core/core.module';
import { LoginPageModule } from './pages/login/login-page.module';
import { OrdersPageModule } from './pages/orders/orders-page.module';
import { SupplierPageModule } from './pages/suppliers/supplier-page.module';
import { OrderDetailsModule } from './pages/order-details/order-details.module';
import { SupplierDetailsModule } from './pages/supplier-details/supplier-details.module';
import { MemberDetailsModule } from './pages/member-details/member-details.module';
import { ProjectsModule } from './pages/projects/projects.module';
import { ProjectDetailsModule } from './pages/project-details/project-details.module';
import { UsersModule } from './pages/users/users.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MembersModule,
    LoginPageModule,
    OrdersPageModule,
    SupplierPageModule,
    AppRoutingModule,
    FormsModule,
    CoreModule,
    OrderDetailsModule,
    SupplierDetailsModule,
    MemberDetailsModule,
    ProjectsModule,
    ProjectDetailsModule,
    UsersModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
