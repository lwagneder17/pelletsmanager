import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import {
  faBars,
  faBriefcase,
  faFolderOpen,
  faUser,
  faUserClock,
  faUsers,
} from '@fortawesome/free-solid-svg-icons';
import { filter, map } from 'rxjs';
import { SidebarService } from './core/sidebar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(){}
}
