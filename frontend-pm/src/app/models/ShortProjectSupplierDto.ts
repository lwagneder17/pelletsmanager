export class ShortProjectSupplierDto{
    id!: number;
    supplierShortForm!: string;
    supplierCompanyName!: string;
    pelletsPrice!: number;
    alpPrice!: number;
    deliveryEnd!: string;
}