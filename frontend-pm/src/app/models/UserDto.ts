export class UserDto{
    id!: number;
    firstName!: string;
    lastName!: string;
    shortForm!: string;
    password!: string;
    active!: boolean;
    creationDate!: Date;
}