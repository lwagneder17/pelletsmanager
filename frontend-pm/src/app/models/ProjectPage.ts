import { ShortProjectDto } from "./ShortProjectDto";

export class ProjectPage{
    pageProjects!: ShortProjectDto[];
    projectCount!: number;
}