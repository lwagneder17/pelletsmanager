export class EditProjectDto{
    active!: boolean;
    shortDescription!: string;
    endOrderDate!: Date;
    path!: string;
    expenses!: number;
    loginPrefix!: string;
    quantityPreviousYear!: number;
    quantityOrdersPreviousYear!: number;
    quantityOrdersPreviousYear0!: number;
    ppiPrice!: number;
    ppiValue!: number;
    ppiDate!: Date;
    history!: string;
}