export class SupplierDto {

    id!: number;
    shortForm!: string;
    companyName!: string;
    contactName!: string;
    active!: boolean;
    plz!: string;
    city!: string;
    street!: string;
    greetingShort!: string;
    greetingLong !: string;
    telephone!: string;
    email!: string;
    web!: string;
    history!: string;
    regDate!: string;
}