export class AddMemberDto{
    title!: string;
    additionalTitle!: string;
    firstName!: string;
    lastName!: string;
    email!: string;
    contactTypeId!: number;
    plz!: string;
    city!: string;
    street!: string;
    deliveryAddress!: string;
    privateNumber!: string;
    secondaryNumber!: string;
    internInfo!: string;
    permanentInfo!: string;
}