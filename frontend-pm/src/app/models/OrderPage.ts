import { ShortOrderDto } from "./ShortOrderDto";

export class OrderPage{
    pageOrders!: ShortOrderDto[];
    orderCount!: number;
}