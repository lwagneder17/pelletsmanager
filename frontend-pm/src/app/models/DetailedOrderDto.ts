import { ContactTypeDto } from "./ContactTypeDto";
import { OrderTypeDto } from "./OrderTypeDto";
import { ShortMemberDto } from "./ShortMemberDto";
import { ShortProjectSupplierDto } from "./ShortProjectSupplierDto";

export class DetailedOrderDto {

    id!: number;
    member!: ShortMemberDto;
    projectSupplier!: ShortProjectSupplierDto;
    orderType!: OrderTypeDto;
    orderDate!: Date;
    amountOrdered!: number;
    shortInfo!: string;
    contactType!: ContactTypeDto
    availableDeliveryDateOptions!: string[];
}