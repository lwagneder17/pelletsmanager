export class ContactTypeStatsDto{
    contactType!: string;
    amountOfOrders!: number;
    shareOfOverallOrderQuantity!: number;
}