import { ContactTypeDto } from "./ContactTypeDto";
import { ShortOrderDto } from "./ShortOrderDto";

export class DetailedMemberDto{
    id!: number;
    contactType!: ContactTypeDto;
    orders!: ShortOrderDto[];
    regDate!: string;
    active!: boolean;
    title!: string;
    additionalTitle!: string;
    firstName!: string;
    lastName!: string;
    street!: string;
    plz!: string;
    city!: string;
    privateNumber!: string;
    secondaryNumber!: string;
    email!: string;
    delAddress!: string;
    permanentInfo!: string;
    internInfo!: string;
    password!: string;
    webId!: string;
}