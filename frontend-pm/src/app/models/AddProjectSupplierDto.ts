export class AddProjectSupplierDto{
    supplierId!: number;
    projectId!: number;
    pelletsPrice!: number;
    alpPrice!: number;
    endOrderDate!: Date;
    minOrderAmount!: number;
}