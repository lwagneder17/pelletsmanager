export class ShortProjectDto{
    id!: number;
    active!: boolean;
    projectName!: string;
    supplierShortCodes!: string;
    orderQuantity!: number;
    amountOfOrders!: number;
    averageOrderQuantity!: number;
    ppiPrice!: number;
    ppiValue!: number;
}