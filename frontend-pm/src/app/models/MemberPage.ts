import { ShortMemberDto } from "./ShortMemberDto";

export class MemberPage{
    pageMembers!: ShortMemberDto[];
    memberCount!: number;
}