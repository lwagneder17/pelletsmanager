export class AddOrderDto {
    userId!: number;
    memberId!: number;
    orderTypeId!: number;
    contactTypeId!: number;
    projectSupplierId!: number;
    amountOrdered!: number;
    wishedDelDate!: string;
    shortInfo!: string;
}