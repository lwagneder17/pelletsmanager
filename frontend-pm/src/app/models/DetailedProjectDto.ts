import { DetailedProjectSupplierDto } from "./DetailedProjectSupplierDto";
import { ShortProjectSupplierDto } from "./ShortProjectSupplierDto";

export class DetailedProjectDto{
    id!: number;
    projectSuppliers!: DetailedProjectSupplierDto[];
    active!: boolean;
    shortDescription!: string;
    endOrderDate!: Date;
    path!: string;
    expenses!: number;
    loginPrefix!: string;
    quantityPreviousYear!: number;
    quantityOrdersPreviousYear!: number;
    quantityOrdersPreviousYear0!: number;
    ppiPrice!: number;
    ppiValue!: number;
    ppiDate!: Date;
    history!: string;
    amountOfOrders!: number;
    amountOfOrders0!: number;
    orderQuantity!: number;
    averageOrderQuantity!: number;
    missingOrders!: number;
    differenceToPreviousYear!: number;
}