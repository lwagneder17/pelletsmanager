import { SupplierDto } from "./SupplierDto";

export class SupplierPage{
    pageSuppliers!: SupplierDto[];
    supplierCount!: number;
}