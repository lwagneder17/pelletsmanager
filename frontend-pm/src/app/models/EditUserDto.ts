export class EditUserDto{
    firstName!: string;
    lastName!: string;
    shortForm!: string;
    password!: string;
    active!: boolean;
}