export class ProjectSupplierStatsDto{
    supplierName!: string;
    orderQuantity!: number;
    amountOfOrders!: number;
    shareOfOverallOrderQuantity!: number;
}