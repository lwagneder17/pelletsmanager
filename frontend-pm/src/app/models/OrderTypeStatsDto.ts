export class OrderTypeStatsDto{
    contactType!: string;
    amountOfOrders!: number;
    shareOfOverallOrderQuantity!: number;
}