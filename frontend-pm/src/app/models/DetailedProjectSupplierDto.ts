export class DetailedProjectSupplierDto{
    id!: number;
    supplierId!: number;
    supplierShortForm!: string;
    supplierName!: string;
    contactName!: string;
    telephoneNumber!: string;
    email!: string;
    pelletsPrice!: number;
    alpPrice!: number;
    deliveryEnd!: Date;
    minDeliveryAmount!: number;
    amountOfOrders!: number;
    orderQuantity!: number;
    averageOrderQuantity!: number;
}