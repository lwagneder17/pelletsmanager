export class AddSupplierDto {
    shortForm!: string;
    companyName!: string;
    contactName!: string;
    greetingShort!: string;
    greetingLong!: string;
    street!: string;
    plz!: string;
    city!: string;
    telephone!: string;
    email!: string;
    web!: string;
    history!: string;
}