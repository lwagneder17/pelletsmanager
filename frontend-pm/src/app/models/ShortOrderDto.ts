import { ShortMemberDto } from "./ShortMemberDto";

export class ShortOrderDto {
    id!: number;
    member!: ShortMemberDto;
    supplierShortForm!: string;
    username!: string;
    wishedDelDate!: string;
    contactType!: string;
    amountOrdered!: number;
    memberConfirmation!: boolean;
    supplierConfirmation!: boolean;
    file!: string;
}