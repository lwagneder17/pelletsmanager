export class EditOrderDto {

    memberId!: number;
    projectSupplierId!: number;
    orderTypeId!: number;
    contactTypeId!: number;
    userId!: number;
    amountOrdered!: number;
    wishedDelDate!: string;
    memberConfirmation!: boolean;
    supplierConfirmation!: boolean;
    shortInfo!: string;
}