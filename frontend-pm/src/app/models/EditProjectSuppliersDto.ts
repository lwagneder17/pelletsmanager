export class EditProjectSupplierDto{
    supplierId!: number;
    projectId!: number;
    pelletsPrice!: number;
    alpPrice!: number;
    deliveryEnd!: Date;
    minDeliveryAmount!: number;
    amountOfOrders!: number;
    orderQuantity!: number;
}