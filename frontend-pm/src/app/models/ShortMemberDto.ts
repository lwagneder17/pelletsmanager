export class ShortMemberDto{
    id!: number;
    contactType!: string;
    regDate!: string ;
    active!: boolean;
    fullName!: string;
    street!: string;
    plz!: string;
    city!: string;
    differentDeliveryAddress!: boolean;
    privateNumber!: string;
    secondaryNumber!: string;
    email!: string;
}