const {app, BrowserWindow} = require('electron');  
const url = require('url');
const path = require('path');   
	
function onReady () {     
	win = new BrowserWindow({show: false, autoHideMenuBar: true})
	win.maximize()
	win.loadURL(url.format({      
		pathname: path.join(
			__dirname,
			'dist/frontend-pm/index.html'),       
		protocol: 'file:',      
		slashes: true     
	}))   
} 

app.on('ready', onReady);