package com.pelletsverein.backendpm.controller;

import com.pelletsverein.backendpm.data.dtos.*;
import com.pelletsverein.backendpm.data.entitys.ContactTypeEntity;
import com.pelletsverein.backendpm.data.entitys.OrderTypeEntity;
import com.pelletsverein.backendpm.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@org.springframework.web.bind.annotation.RestController
@RequestMapping(value="/pelletsBackend")
public class RestController {
    @Autowired
    private DataService dataService;

    @RequestMapping(value="/login", method = RequestMethod.POST)
    public HttpEntity<UserDto> loginAuth(@RequestBody LoginDto loginDto){
        return new HttpEntity<>(dataService.authenticateLogin(loginDto));
    }

    @RequestMapping(value = "members", method = RequestMethod.GET)
    public HttpEntity<MemberPage> getMembers(
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize,
            @RequestParam(name = "pageNr", required = false, defaultValue = "1") int pageNr,
            @RequestParam(name = "filterOption", required = false, defaultValue = "0") int filterId,
            @RequestParam(name = "searchOption", required = false, defaultValue = "0") int searchId,
            @RequestParam(name = "searchString", required = false, defaultValue = "") String searchString
    ){
        return new HttpEntity<>(dataService.getMembers(pageSize, pageNr, filterId, searchId, searchString));
    }

    @RequestMapping(value="/members/member/{memberId}", method = RequestMethod.GET)
    public HttpEntity<DetailedMemberDto> getMember(
            @PathVariable int memberId){
        return new HttpEntity<>(dataService.getMember(memberId));
    }

    @RequestMapping(value="/members", method = RequestMethod.POST)
    public HttpEntity<ShortMemberDto> addMember(@RequestBody AddMemberDto addMemberDto){
        return new HttpEntity<>(dataService.addNewMember(addMemberDto));
    }

    @RequestMapping(value="/members/member/{memberId}", method = RequestMethod.PUT)
    public HttpEntity<DetailedMemberDto> editMember(@PathVariable int memberId, @RequestBody EditMemberDto memberDto){
        return new HttpEntity<>(dataService.editMember(memberId, memberDto));
    }

    @RequestMapping(value="/members/member/{memberId}", method = RequestMethod.DELETE)
    public HttpEntity<String> deleteMember(@PathVariable int memberId){
        return new HttpEntity<>(dataService.deleteMember(memberId));
    }

    @RequestMapping(value="/orders", method = RequestMethod.GET)
    public HttpEntity<OrderPage> getOrders(
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize,
            @RequestParam(name = "pageNr", required = false, defaultValue = "1") int pageNr,
            @RequestParam(name = "searchString", required = false, defaultValue = "") String searchString
            ){
        return new HttpEntity<>(dataService.getOrders(pageSize, pageNr, searchString));
    }

    @RequestMapping(value="/orders/order/{orderId}", method = RequestMethod.GET)
    public HttpEntity<DetailedOrderDto> getOrders(@PathVariable int orderId){
        return new HttpEntity<>(dataService.getOrder(orderId));
    }

    @RequestMapping(value="/orders", method = RequestMethod.POST)
    public HttpEntity<ShortOrderDto> getOrders(@RequestBody AddOrderDto addOrderDto){
        return new HttpEntity<>(dataService.addNewOrder(addOrderDto));
    }

    @RequestMapping(value="/orders/order/{orderId}", method = RequestMethod.PUT)
    public HttpEntity<DetailedOrderDto> getOrders(@PathVariable int orderId, @RequestBody EditOrderDto editOrderDto){
        return new HttpEntity<>(dataService.editOrder(orderId, editOrderDto));
    }

    @RequestMapping(value="/orders/order/{orderId}", method = RequestMethod.DELETE)
    public HttpEntity<String> deleteOrder(@PathVariable int orderId){
        return new HttpEntity<>(dataService.deleteOrder(orderId));
    }

    @RequestMapping(value="suppliers", method = RequestMethod.GET)
    public HttpEntity<SupplierPage> getSuppliers(
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize,
            @RequestParam(name = "pageNr", required = false, defaultValue = "1") int pageNr,
            @RequestParam(name = "searchString", required = false, defaultValue = "") String searchString
    ){
        return new HttpEntity<>(dataService.getSuppliers(pageSize, pageNr));
    }

    @RequestMapping(value="suppliers/supplier/{supplierId}", method = RequestMethod.GET)
    public HttpEntity<SupplierDto> getSupplier(@PathVariable int supplierId){
        return new HttpEntity<>(dataService.getSupplier(supplierId));
    }

    @RequestMapping(value="suppliers", method = RequestMethod.POST)
    public HttpEntity<SupplierDto> getSupplier(@RequestBody AddSupplierDto addSupplierDto){
        return new HttpEntity<>(dataService.addSupplier(addSupplierDto));
    }

    @RequestMapping(value="suppliers/supplier/{supplierId}", method = RequestMethod.PUT)
    public HttpEntity<SupplierDto> editSupplier(@PathVariable int supplierId, @RequestBody EditSupplierDto editSupplierDto){
        return new HttpEntity<>(dataService.editSupplier(supplierId, editSupplierDto));
    }

    @RequestMapping(value="suppliers/supplier/{supplierId}", method = RequestMethod.DELETE)
    public HttpEntity<String> deleteSupplier(@PathVariable int supplierId){
        return new HttpEntity<>(dataService.deleteSupplier(supplierId));
    }

    @RequestMapping(value="projects", method = RequestMethod.GET)
    public HttpEntity<ProjectPage> getProjects(
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize,
            @RequestParam(name = "pageNr", required = false, defaultValue = "1") int pageNr
    ){
        return new HttpEntity<>(dataService.getProjects(pageSize, pageNr));
    }

    @RequestMapping(value="projects/project/{projectId}", method = RequestMethod.GET)
    public HttpEntity<DetailedProjectDto> getProject(@PathVariable int projectId){
        return new HttpEntity<>(dataService.getProject(projectId));
    }

    @RequestMapping(value="projects", method = RequestMethod.POST)
    public HttpEntity<ShortProjectDto> addProject(@RequestBody AddProjectDto addProjectDto) {
        return new HttpEntity<>(dataService.addProjects(addProjectDto));
    }
    @RequestMapping(value="projects/project/{projectId}", method = RequestMethod.PUT)
    public HttpEntity<DetailedProjectDto> editProject(@PathVariable int projectId, @RequestBody EditProjectDto editProjectDto) {
        return new HttpEntity<>(dataService.editProject(projectId, editProjectDto));
    }

    @RequestMapping(value="projects/project/{projectId}", method = RequestMethod.DELETE)
    public HttpEntity<String> deleteProject(@PathVariable int projectId){
        return new HttpEntity<>(dataService.deleteProject(projectId));
    }

    @RequestMapping(value="projectsuppliers", method = RequestMethod.GET)
    public HttpEntity<List<ShortProjectSupplierDto>> getProjectSuppliers(){
        return new HttpEntity<>(dataService.getProjectSuppliers());
    }

    @RequestMapping(value="projectsuppliers/projectsupplier/{projectSupplierId}", method = RequestMethod.GET)
    public HttpEntity<DetailedProjectSupplierDto> getProjectSupplier(@PathVariable int projectSupplierId){
        return new HttpEntity<>(dataService.getProjectSupplier(projectSupplierId));
    }

    @RequestMapping(value="projectsuppliers", method = RequestMethod.POST)
    public HttpEntity<DetailedProjectSupplierDto> getProjectSupplier(@RequestBody AddProjectSupplierDto addProjectSupplierDto){
        return new HttpEntity<>(dataService.addProjectSupplier(addProjectSupplierDto));
    }

    @RequestMapping(value="projectsuppliers/projectsupplier/{projectSupplierId}", method = RequestMethod.PUT)
    public HttpEntity<DetailedProjectSupplierDto> editProjectSupplier(@PathVariable int projectSupplierId, @RequestBody EditProjectSupplierDto editProjectSupplierDto){
        return new HttpEntity<>(dataService.editProjectSupplier(projectSupplierId, editProjectSupplierDto));
    }

    @RequestMapping(value="projectsuppliers/projectsupplier/{projectSupplierId}", method = RequestMethod.DELETE)
    public HttpEntity<String> deleteProjectSupplier(@PathVariable int projectSupplierId){
        return new HttpEntity<>(dataService.deleteProjectSupplier(projectSupplierId));
    }

    @RequestMapping(value = "members/filterOptions", method = RequestMethod.GET)
    public HttpEntity<List<String>> getMemberFilterOptions(){
        return new HttpEntity<>(dataService.getMemberFilterOptions());
    }

    @RequestMapping(value = {"orders/search/{searchString}", "orders/search/"}, method = RequestMethod.GET)
    public HttpEntity<List<ShortOrderDto>> getOrdersByDefaultSearch(@PathVariable(required = false) String searchString){
        return new HttpEntity<>(dataService.getOrdersByDefaultSearch(searchString));
    }

    @RequestMapping(value = "suppliers/filterOptions", method = RequestMethod.GET)
    public HttpEntity<List<String>> getSupplierFilterOptions(){
        return new HttpEntity<>(dataService.getSupplierFilterOptions());
    }

    @RequestMapping(value = "suppliers/suppliersByFilterOption/{filterOptionId}", method = RequestMethod.GET)
    public HttpEntity<List<SupplierDto>> getSuppliersByFilterOption(@PathVariable int filterOptionId){
        return new HttpEntity<>(dataService.getSuppliersByFilterOption(filterOptionId));
    }

    @RequestMapping(value = "contactTypes", method = RequestMethod.GET)
    public HttpEntity<List<ContactTypeEntity>> getContactTypes(){
        return new HttpEntity<>(dataService.getContactTypes());
    }

    @RequestMapping(value = "members/member/{memberId}/orders", method = RequestMethod.GET)
    public HttpEntity<List<ShortOrderDto>> getOrdersOfMember(@PathVariable int memberId){
        return new HttpEntity<>(dataService.getOrdersOfMember(memberId));
    }

    @RequestMapping(value = "ordertypes", method = RequestMethod.GET)
    public HttpEntity<List<OrderTypeEntity>> getOrderTypes(){
        return new HttpEntity<>(dataService.getOrderTypes());
    }

    @RequestMapping(value = "availabledeliverydates/{projectSupplierId}", method = RequestMethod.GET)
    public HttpEntity<List<String>> getAvailableDeliveryDates(@PathVariable int projectSupplierId){
        return new HttpEntity<>(dataService.getAvailableDeliveryDates(projectSupplierId));
    }

    @RequestMapping(value = "project/{projectId}/availableSuppliers", method = RequestMethod.GET)
    public HttpEntity<List<SupplierDto>> getAvailableSuppliersForProject(@PathVariable int projectId){
        return new HttpEntity<>(dataService.getAvailableSuppliersForProject(projectId));
    }

    @RequestMapping(value = "project/{projectId}/projectSupplierStats", method = RequestMethod.GET)
    public HttpEntity<List<ProjectSupplierStatsDto>> getProjectSupplierStats(@PathVariable int projectId){
        return new HttpEntity<>(dataService.getProjectSupplierStats(projectId));
    }

    @RequestMapping(value = "project/{projectId}/contactTypeStats", method = RequestMethod.GET)
    public HttpEntity<List<ContactTypeStatsDto>> getContactTypeStats(@PathVariable int projectId){
        return new HttpEntity<>(dataService.getContactTypeStats(projectId));
    }

    @RequestMapping(value = "project/{projectId}/orderTypeStats", method = RequestMethod.GET)
    public HttpEntity<List<OrderTypeStatsDto>> getOrderTypeStats(@PathVariable int projectId){
        return new HttpEntity<>(dataService.getOrderTypeStats(projectId));
    }

    @RequestMapping(value = "users", method = RequestMethod.GET)
    public HttpEntity<UserPage> getUsers(
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize,
            @RequestParam(name = "pageNr", required = false, defaultValue = "1") int pageNr
    ){
        return new HttpEntity<>(dataService.getUsers(pageSize, pageNr));
    }

    @RequestMapping(value = "users", method = RequestMethod.POST)
    public HttpEntity<UserPage> addUser(
            @RequestBody AddUserDto newUser,
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize,
            @RequestParam(name = "pageNr", required = false, defaultValue = "1") int pageNr
    ){
        dataService.addUser(newUser);
        return new HttpEntity<>(dataService.getUsers(pageSize, pageNr));
    }

    @RequestMapping(value = "users/user/{userId}", method = RequestMethod.DELETE)
    public HttpEntity<UserPage> deleteUser(
            @PathVariable int userId,
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize,
            @RequestParam(name = "pageNr", required = false, defaultValue = "1") int pageNr
    ){
        dataService.deleteUser(userId);
        return new HttpEntity<>(dataService.getUsers(pageSize, pageNr));
    }

    @RequestMapping(value = "users/user/{userId}", method = RequestMethod.PUT)
    public HttpEntity<UserDto> updateUser(@PathVariable int userId, @RequestBody EditUserDto editUser){
        return new HttpEntity<>(dataService.editUser(userId, editUser));
    }
}
