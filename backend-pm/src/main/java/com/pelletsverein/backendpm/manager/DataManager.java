package com.pelletsverein.backendpm.manager;

import com.pelletsverein.backendpm.data.classes.*;
import com.pelletsverein.backendpm.data.database.*;
import com.pelletsverein.backendpm.data.dtos.AddUserDto;
import com.pelletsverein.backendpm.data.dtos.EditUserDto;
import com.pelletsverein.backendpm.data.dtos.LoginDto;
import com.pelletsverein.backendpm.data.entitys.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class DataManager {

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ContactTypeRepository contactTypeRepository;

    @Autowired
    private LoggingRepository loggingRepository;

    @Autowired
    private OrderTypeRepository orderTypeRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectSupplierRepository projectSupplierRepository;

    @Autowired
    private SupplierRepository supplierRepository;

    //region Login
    public UserEntity authenticateLogin(LoginDto loginDto) {
        return userRepository.findByShortFormEqualsIgnoreCaseAndPassword(loginDto.getUsername(), loginDto.getPassword());
    }
    //endregion

    //region Member
    public List<MemberEntity> getMembers(){
        List<MemberEntity> results = new ArrayList<>();
        memberRepository.findAll().forEach(results::add);
        return results;
    }

    public MemberEntity getMemberById(int id) {
        if(memberRepository.findById(id).isPresent()){
            return memberRepository.findById(id).get();
        }
        return null;
    }

    public ContactTypeEntity getContactTypeById(int contactTypeId) {
        if(contactTypeRepository.findById(contactTypeId).isPresent()){
            return contactTypeRepository.findById(contactTypeId).get();
        }
        return null;
    }

    public MemberEntity addMember(Member member) {
        return memberRepository.save(convertMemberToEntity(new MemberEntity(), member));
    }

    public MemberEntity editMember(Member member) {
        if(memberRepository.findById(member.getId()).isPresent()){
            return memberRepository.save(convertMemberToEntity(memberRepository.findById(member.getId()).get(), member));
        }
        return null;
    }

    public String deleteMember(int memberId) {
        if(memberRepository.findById(memberId).isPresent()){
            memberRepository.deleteById(memberId);
            return "success";
        }
        return "failed";
    }

    public List<OrderEntity> getOrders() {
        List<OrderEntity> results = new ArrayList<>();
        orderRepository.findAll().forEach(results::add);
        return results;
    }

    public OrderEntity getOrderById(int orderId) {
        if(orderRepository.findById(orderId).isPresent()){
            return orderRepository.findById(orderId).get();
        }
        return null;
    }

    public OrderEntity addOrder(Order order) {
        return orderRepository.save(convertOrderToEntity(new OrderEntity(), order));
    }

    public OrderEntity editOrder(int orderId, Order order) {
        if(orderRepository.findById(orderId).isPresent()){
            return orderRepository.save(convertOrderToEntity(orderRepository.findById(orderId).get(), order));
        }
        return null;
    }

    public String deleteOrder(int orderId) {
        if(orderRepository.findById(orderId).isPresent()){
            orderRepository.deleteById(orderId);
            return "success";
        }
        return "failed";
    }

    public List<SupplierEntity> getSuppliers() {
        List<SupplierEntity> results = new ArrayList<>();
        supplierRepository.findAll().forEach(results::add);
        return results;
    }

    public SupplierEntity getSupplierById(int supplierId) {
        if(supplierRepository.findById(supplierId).isPresent()){
            return supplierRepository.findById(supplierId).get();
        }
        return null;
    }

    public SupplierEntity addSupplier(Supplier supplier) {
        return supplierRepository.save(convertSupplierToEntity(new SupplierEntity(), supplier));
    }

    public SupplierEntity editSupplier(int supplierId, Supplier supplier) {
        if(supplierRepository.findById(supplierId).isPresent()){
            return supplierRepository.save(convertSupplierToEntity(supplierRepository.findById(supplierId).get(), supplier));
        }
        return null;
    }

    public String deleteSupplier(int supplierId) {
        if(supplierRepository.findById(supplierId).isPresent()){
            supplierRepository.deleteById(supplierId);
            return "success";
        }
        return "failed";
    }

    public List<ProjectEntity> getProjects() {
        List<ProjectEntity> results = new ArrayList<>();
        projectRepository.findAll().forEach(results::add);
        return results;
    }

    public ProjectEntity getProjectById(int projectId) {
        if(projectRepository.findById(projectId).isPresent()) {
            return projectRepository.findById(projectId).get();
        }
        return null;
    }

    public ProjectEntity addProject(Project project) {
        return projectRepository.save(convertProjectToEntity(new ProjectEntity(), project));
    }

    public ProjectEntity editProject(int projectId, Project project) {
        if(projectRepository.findById(projectId).isPresent()){
            return projectRepository.save(convertProjectToEntity(projectRepository.findById(projectId).get(), project));
        }
        return null;
    }

    public String deleteProject(int projectId) {
        if(projectRepository.findById(projectId).isPresent()){
            projectRepository.deleteById(projectId);
            return "success";
        }
        return "failed";
    }

    public List<ProjectSupplierEntity> getProjectSuppliers() {
        List<ProjectSupplierEntity> results = new ArrayList<>();
        projectSupplierRepository.findAll().forEach(results::add);
        return results;
    }

    public ProjectSupplierEntity addProjectSupplier(ProjectSupplier projectSupplier) {
        return projectSupplierRepository.save(convertProjectSupplierToEntity(new ProjectSupplierEntity(), projectSupplier));
    }

    public ProjectSupplierEntity editProjectSupplier(int projectSupplierId, ProjectSupplier projectSupplier) {
        if(projectSupplierRepository.findById(projectSupplierId).isPresent()){
            return projectSupplierRepository.save(convertProjectSupplierToEntity(projectSupplierRepository.findById(projectSupplierId).get(), projectSupplier));
        }
        return null;
    }

    public String deleteProjectSupplier(int projectSupplierId) {
        if(projectSupplierRepository.findById(projectSupplierId).isPresent()){
            projectSupplierRepository.deleteById(projectSupplierId);
            return "success";
        }
        return "failed";
    }

    public List<ContactTypeEntity> getContactTypes(){
        List<ContactTypeEntity> results = new ArrayList<>();
        contactTypeRepository.findAll().forEach(results::add);
        return results;
    }

    public List<UserEntity> getUsers(){
        List<UserEntity> results = new ArrayList<>();
        userRepository.findAll().forEach(results::add);
        return results;
    }

    public void addUser(User newUser) {
        userRepository.save(convertUserToEntity(new UserEntity(), newUser));
    }

    public void deleteUser(int userId){
        userRepository.deleteById(userId);
    }

    public UserEntity editUser(int userId, User editUser) {
        return userRepository.save(convertUserToEntity(userRepository.findById(userId).get(), editUser));
    }

    private UserEntity convertUserToEntity(UserEntity user, User newUser) {
        user.setLastName(newUser.getLastName());
        user.setFirstName(newUser.getFirstName());
        user.setActive(newUser.isActive());
        user.setPassword(newUser.getPassword());
        user.setShortForm(newUser.getShortForm());
        if(newUser.getCreationDate() != null) user.setCreationDate(newUser.getCreationDate());

        return user;
    }

    private ProjectSupplierEntity convertProjectSupplierToEntity(ProjectSupplierEntity projectSupplierEntity, ProjectSupplier projectSupplier) {
        projectSupplierEntity.setSupplierEntity(getSupplierById(projectSupplier.getSupplierId()));
        projectSupplierEntity.setProjectEntity(getProjectById(projectSupplier.getProjectId()));
        if(projectSupplierEntity.getOrders() == null) projectSupplierEntity.setOrders(new HashSet<>());
        projectSupplierEntity.setPelletsPrice(projectSupplier.getPelletsPrice());
        projectSupplierEntity.setAlpPrice(projectSupplier.getAlpPrice());
        projectSupplierEntity.setDeliveryEnd(projectSupplier.getDeliveryEnd());
        projectSupplierEntity.setMinDeliveryAmount(projectSupplier.getMinDeliveryAmount());
        projectSupplierEntity.setAmountOfOrders(projectSupplier.getAmountOfOrders());
        projectSupplierEntity.setOrderQuantity(projectSupplier.getOrderQuantity());

        return projectSupplierEntity;
    }
    private ProjectEntity convertProjectToEntity(ProjectEntity projectEntity, Project project) {
        projectEntity.setActive(project.isActive());
        if(projectEntity.getProjectSupplierEntities() == null) projectEntity.setProjectSupplierEntities(new HashSet<>());
        projectEntity.setShortDescription(project.getShortDescription());
        projectEntity.setEndOrderDate(project.getEndOrderDate());
        projectEntity.setPath(project.getPath());
        projectEntity.setExpenses(project.getExpenses());
        projectEntity.setLoginPrefix(project.getLoginPrefix());
        projectEntity.setQuantityPreviousYear(project.getQuantityPreviousYear());
        projectEntity.setQuantityOrdersPreviousYear(project.getQuantityOrdersPreviousYear());
        projectEntity.setQuantityOrdersPreviousYear0(project.getQuantityOrdersPreviousYear0());
        projectEntity.setPpiPrice(project.getPpiPrice());
        projectEntity.setPpiValue(project.getPpiValue());
        projectEntity.setPpiDate(project.getPpiDate());
        projectEntity.setHistory(project.getHistory());

        return projectEntity;
    }
    public ProjectSupplierEntity getProjectSupplierById(int projectSupplierId){
        if(projectSupplierRepository.findById(projectSupplierId).isPresent()){
            return projectSupplierRepository.findById(projectSupplierId).get();
        }
        return null;
    }
    public UserEntity getUserById(int userId){
        if(userRepository.findById(userId).isPresent()){
            return userRepository.findById(userId).get();
        }
        return null;
    }
    public OrderTypeEntity getOrderTypeById(int orderTypeId){
        if(orderTypeRepository.findById(orderTypeId).isPresent()){
            return orderTypeRepository.findById(orderTypeId).get();
        }
        return null;
    }

    public List<OrderTypeEntity> getOrderTypes() {
        List<OrderTypeEntity> results = new ArrayList<>();
        orderTypeRepository.findAll().forEach(results::add);
        return results;
    }
    private OrderEntity convertOrderToEntity(OrderEntity orderEntity, Order order) {
        orderEntity.setMemberEntity(getMemberById(order.getMemberId()));
        orderEntity.setProjectSupplierEntity(getProjectSupplierById(order.getProjectSupplierId()));
        orderEntity.setUserEntity(getUserById(order.getUserId()));
        orderEntity.setOrderTypeEntity(getOrderTypeById(order.getOrderTypeId()));
        orderEntity.setContactTypeEntity(getContactTypeById(order.getContactTypeId()));
        if(order.getOrderDate() != null) orderEntity.setOrderDate(order.getOrderDate());
        orderEntity.setAmountOrdered(order.getAmountOrdered());
        orderEntity.setWishedDelDate(order.getWishedDelDate());
        orderEntity.setMemberConfirmation(order.getMemberConfirmation());
        orderEntity.setSupplierConfirmation(order.getSupplierConfirmation());
        orderEntity.setShortInfo(order.getShortInfo());
        orderEntity.setFile(order.getFile());

        return orderEntity;
    }
    private MemberEntity convertMemberToEntity(MemberEntity memberEntity, Member member){
        memberEntity.setActive(member.isActive());
        memberEntity.setAdditionalTitle(member.getAdditionalTitle());
        memberEntity.setContactTypeEntity(getContactTypeById(member.getContactTypeId()));
        if(memberEntity.getOrders() == null) memberEntity.setOrders(new HashSet<>());
        memberEntity.setCity(member.getCity());
        if(member.getRegDate() != null) memberEntity.setRegDate(member.getRegDate());
        memberEntity.setEmail(member.getEmail());
        memberEntity.setFirstName(member.getFirstName());
        memberEntity.setLastName(member.getLastName());
        memberEntity.setDelAddress(member.getDelAddress());
        memberEntity.setInternInfo(member.getInternInfo());
        memberEntity.setTitle(member.getTitle());
        memberEntity.setPlz(member.getPlz());
        memberEntity.setStreet(member.getStreet());
        memberEntity.setPrivateNumber(member.getPrivateNumber());
        memberEntity.setSecondaryNumber(member.getSecondaryNumber());
        memberEntity.setPermanentInfo(member.getPermanentInfo());
        memberEntity.setPassword(member.getPassword());
        memberEntity.setWebId(member.getWebId());

        return memberEntity;
    }
    private SupplierEntity convertSupplierToEntity(SupplierEntity supplierEntity, Supplier supplier) {
        supplierEntity.setShortForm(supplier.getShortForm());
        if(supplier.getRegDate() != null) supplierEntity.setRegDate(supplier.getRegDate());
        supplierEntity.setActive(supplier.isActive());
        supplierEntity.setCompanyName(supplier.getCompanyName());
        supplierEntity.setContactName(supplier.getContactName());
        supplierEntity.setGreetingShort(supplier.getGreetingShort());
        supplierEntity.setGreetingLong(supplier.getGreetingLong());
        supplierEntity.setStreet(supplier.getStreet());
        supplierEntity.setPlz(supplier.getPlz());
        supplierEntity.setCity(supplier.getCity());
        supplierEntity.setTelephone(supplier.getTelephone());
        supplierEntity.setEmail(supplier.getEmail());
        supplierEntity.setWeb(supplier.getWeb());
        supplierEntity.setHistory(supplier.getHistory());

        return supplierEntity;
    }
    //endregion
}
