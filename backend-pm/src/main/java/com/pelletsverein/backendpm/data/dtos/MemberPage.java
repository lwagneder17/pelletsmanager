package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;
import java.util.List;

@Data
public class MemberPage {
    private List<ShortMemberDto> pageMembers;
    private int memberCount;
}
