package com.pelletsverein.backendpm.data.classes;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

@Data
public class Member {
    private int id;
    private int contactTypeId;
    private LocalDateTime regDate;
    private boolean active;
    private String title;
    private String additionalTitle;
    private String firstName;
    private String lastName;
    private String street;
    private String plz;
    private String city;
    private String privateNumber;
    private String secondaryNumber;
    private String email;
    private String delAddress;
    private String permanentInfo;
    private String internInfo;
    private String password;
    private String webId;
}
