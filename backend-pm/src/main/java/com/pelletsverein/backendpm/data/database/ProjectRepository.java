package com.pelletsverein.backendpm.data.database;

import com.pelletsverein.backendpm.data.entitys.ProjectEntity;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.criteria.CriteriaBuilder;

public interface ProjectRepository extends CrudRepository<ProjectEntity, Integer> {
}
