package com.pelletsverein.backendpm.data.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SupplierDto {
    private int id;
    private String shortForm;

    @JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
    private LocalDateTime regDate;

    private boolean active;
    private String companyName;
    private String contactName;
    private String greetingShort;
    private String greetingLong;
    private String street;
    private String plz;
    private String city;
    private String telephone;
    private String email;
    private String web;
    private String history;
}
