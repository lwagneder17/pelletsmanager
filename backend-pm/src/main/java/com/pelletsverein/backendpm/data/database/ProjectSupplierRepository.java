package com.pelletsverein.backendpm.data.database;

import com.pelletsverein.backendpm.data.entitys.ProjectSupplierEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProjectSupplierRepository extends CrudRepository<ProjectSupplierEntity, Integer> {
}
