package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

@Data
public class EditMemberDto {
    private int contactTypeId;
    private boolean active;
    private String title;
    private String additionalTitle;
    private String firstName;
    private String lastName;
    private String street;
    private String plz;
    private String city;
    private String privateNumber;
    private String secondaryNumber;
    private String email;
    private String delAddress;
    private String permanentInfo;
    private String internInfo;
    private String password;
    private String webId;
}
