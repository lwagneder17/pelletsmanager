package com.pelletsverein.backendpm.data.database;

import com.pelletsverein.backendpm.data.entitys.MemberEntity;
import org.springframework.data.repository.CrudRepository;

public interface MemberRepository extends CrudRepository<MemberEntity, Integer> {
}
