package com.pelletsverein.backendpm.data.dtos;

import lombok.Getter;

@Getter
public class AddOrderDto {
    private int userId;
    private int memberId;
    private int orderTypeId;
    private int contactTypeId;
    private int projectSupplierId;
    private int amountOrdered;
    private String wishedDelDate;
    private String shortInfo;
}
