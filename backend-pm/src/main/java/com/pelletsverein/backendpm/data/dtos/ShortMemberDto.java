package com.pelletsverein.backendpm.data.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ShortMemberDto {
    private int id;
    private String contactType;

    @JsonFormat(pattern="dd.MM.yyyy HH:mm")
    private LocalDateTime regDate;

    private boolean active;
    private String fullName;
    private String street;
    private String plz;
    private String city;
    private boolean differentDeliveryAddress;
    private String privateNumber;
    private String secondaryNumber;
    private String email;
}
