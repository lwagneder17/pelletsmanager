package com.pelletsverein.backendpm.data.dtos;

import lombok.Getter;

import java.util.Date;

@Getter
public class AddProjectSupplierDto {
    private int supplierId;
    private int projectId;
    private float pelletsPrice;
    private float alpPrice;
    private Date endOrderDate;
    private int minOrderAmount;
}
