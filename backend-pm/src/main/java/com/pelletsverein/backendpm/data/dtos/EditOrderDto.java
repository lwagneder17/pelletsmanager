package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

@Data
public class EditOrderDto {
    private int memberId;
    private int projectSupplierId;
    private int orderTypeId;
    private int contactTypeId;
    private int userId;
    private int amountOrdered;
    private String wishedDelDate;
    private boolean memberConfirmation;
    private boolean supplierConfirmation;
    private String shortInfo;
}
