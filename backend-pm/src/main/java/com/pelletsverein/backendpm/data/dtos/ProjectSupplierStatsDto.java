package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;
import lombok.Setter;

@Data
public class ProjectSupplierStatsDto {
    private String supplierName;
    private double orderQuantity;
    private int amountOfOrders;
    private double shareOfOverallOrderQuantity;
}
