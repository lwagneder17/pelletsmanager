package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

import java.util.List;

@Data
public class SupplierPage {
    private List<SupplierDto> pageSuppliers;
    private int supplierCount;
}
