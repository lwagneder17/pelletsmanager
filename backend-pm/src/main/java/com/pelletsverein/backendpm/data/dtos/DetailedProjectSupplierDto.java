package com.pelletsverein.backendpm.data.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class DetailedProjectSupplierDto {
    private int id;
    private int supplierId;
    private String supplierShortForm;
    private String supplierName;
    private String contactName;
    private String telephoneNumber;
    private String email;
    private float pelletsPrice;
    private float alpPrice;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date deliveryEnd;

    private float minDeliveryAmount;
    private int amountOfOrders;
    private float orderQuantity;
    private float averageOrderQuantity;
}
