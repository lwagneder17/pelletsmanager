package com.pelletsverein.backendpm.data.classes;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class Supplier {
    private int id;
    private String shortForm;
    private LocalDateTime regDate;
    private boolean active;
    private String companyName;
    private String contactName;
    private String greetingShort;
    private String greetingLong;
    private String street;
    private String plz;
    private String city;
    private String telephone;
    private String email;
    private String web;
    private String history;
}
