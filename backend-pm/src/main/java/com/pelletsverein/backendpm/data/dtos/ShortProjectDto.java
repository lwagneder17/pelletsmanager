package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

@Data
public class ShortProjectDto {
    private int id;
    private boolean active;
    private String projectName;
    String supplierShortCodes;
    private double orderQuantity;
    private double amountOfOrders;
    private double averageOrderQuantity;
    private float ppiPrice;
    private float ppiValue;
}
