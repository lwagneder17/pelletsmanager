package com.pelletsverein.backendpm.data.database;

import com.pelletsverein.backendpm.data.entitys.ContactTypeEntity;
import org.springframework.data.repository.CrudRepository;

public interface ContactTypeRepository extends CrudRepository<ContactTypeEntity, Integer> {
}
