package com.pelletsverein.backendpm.data.entitys;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "supplier")
public class SupplierEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String shortForm;
    private LocalDateTime regDate;
    private boolean active;
    private String companyName;
    private String contactName;
    private String greetingShort;
    private String greetingLong;
    private String street;
    private String plz;
    private String city;
    private String telephone;
    private String email;
    private String web;
    private String history;
}
