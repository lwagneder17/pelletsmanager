package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;
import lombok.Setter;

@Data
public class ContactTypeStatsDto {
    private String contactType;
    private int amountOfOrders;
    private double shareOfOverallOrderQuantity;
}
