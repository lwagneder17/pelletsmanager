package com.pelletsverein.backendpm.data.database;

import com.pelletsverein.backendpm.data.entitys.OrderEntity;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<OrderEntity, Integer> {
}
