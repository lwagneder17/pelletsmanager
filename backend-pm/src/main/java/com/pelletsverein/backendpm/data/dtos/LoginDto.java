package com.pelletsverein.backendpm.data.dtos;

import lombok.Getter;

@Getter
public class LoginDto {
    private String username;
    private String password;
}
