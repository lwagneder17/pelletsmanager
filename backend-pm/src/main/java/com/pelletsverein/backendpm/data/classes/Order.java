package com.pelletsverein.backendpm.data.classes;

import com.pelletsverein.backendpm.data.dtos.DetailedMemberDto;
import com.pelletsverein.backendpm.data.entitys.*;
import lombok.Data;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class Order {
    private int id;
    private int memberId;
    private int projectSupplierId;
    private int userId;
    private int orderTypeId;
    private int contactTypeId;
    private LocalDateTime orderDate;
    private int amountOrdered;
    private String wishedDelDate;
    private LocalDateTime memberConfirmation;
    private LocalDateTime supplierConfirmation;
    private String shortInfo;
    private String file;
}
