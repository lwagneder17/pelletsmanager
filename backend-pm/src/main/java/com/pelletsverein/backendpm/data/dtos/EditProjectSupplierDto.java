package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class EditProjectSupplierDto {
    private int supplierId;
    private int projectId;
    private float pelletsPrice;
    private float alpPrice;
    private Date deliveryEnd;
    private int minDeliveryAmount;
    private int amountOfOrders;
    private float orderQuantity;
}
