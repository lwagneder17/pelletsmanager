package com.pelletsverein.backendpm.data.database;

import com.pelletsverein.backendpm.data.entitys.OrderTypeEntity;
import org.springframework.data.repository.CrudRepository;

public interface OrderTypeRepository extends CrudRepository<OrderTypeEntity, Integer> {
}
