package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

import java.util.List;

@Data
public class OrderPage {
    private List<ShortOrderDto> pageOrders;
    private int orderCount;
}
