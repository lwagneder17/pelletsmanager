package com.pelletsverein.backendpm.data.entitys;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "member")
public class MemberEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "message_type", referencedColumnName = "id")
    private ContactTypeEntity contactTypeEntity;

    @OneToMany(mappedBy = "memberEntity", fetch = FetchType.EAGER)
    private Set<OrderEntity> orders;

    private LocalDateTime regDate;
    private boolean active;
    private String title;
    private String additionalTitle;
    private String firstName;
    private String lastName;
    private String street;
    private String plz;
    private String city;
    private String privateNumber;
    private String secondaryNumber;
    private String email;
    private String delAddress;
    private String permanentInfo;
    private String internInfo;
    private String password;
    private String webId;
}
