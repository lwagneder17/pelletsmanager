package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class EditProjectDto {
    private boolean active;
    private String shortDescription;
    private Date endOrderDate;
    private String path;
    private float expenses;
    private String loginPrefix;
    private int quantityPreviousYear;
    private int quantityOrdersPreviousYear;
    private int quantityOrdersPreviousYear0;
    private float ppiPrice;
    private float ppiValue;
    private Date ppiDate;
    private String history;
}
