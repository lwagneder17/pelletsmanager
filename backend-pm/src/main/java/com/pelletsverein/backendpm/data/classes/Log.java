package com.pelletsverein.backendpm.data.classes;

import com.pelletsverein.backendpm.data.dtos.DetailedMemberDto;
import lombok.Data;
import java.time.LocalDateTime;

@Data
public class Log {
    private int id;
    private Project projectEntity;
    private DetailedMemberDto memberEntity;
    private Order orderEntity;
    private LocalDateTime logDate;
    private String logType;
    private String messageType;
    private String messageStatus;
    private String messageReceiver;
}
