package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

import java.util.List;

@Data
public class ProjectPage {
    private List<ShortProjectDto> pageProjects;
    private int projectCount;
}
