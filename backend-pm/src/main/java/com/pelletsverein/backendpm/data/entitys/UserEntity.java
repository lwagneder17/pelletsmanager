package com.pelletsverein.backendpm.data.entitys;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "user")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(mappedBy = "userEntity")
    private Set<OrderEntity> orders;

    private String firstName;
    private String lastName;
    private String shortForm;
    private String password;
    private boolean active;
    private Date creationDate;
}
