package com.pelletsverein.backendpm.data.database;

import com.pelletsverein.backendpm.data.entitys.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    UserEntity findByShortFormEqualsIgnoreCaseAndPassword(String shortForm, String password);
}