package com.pelletsverein.backendpm.data.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
public class DetailedProjectDto {
    private int id;
    private List<DetailedProjectSupplierDto> projectSuppliers;
    private boolean active;
    private String shortDescription;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endOrderDate;

    private String path;
    private float expenses;
    private String loginPrefix;
    private int quantityPreviousYear;
    private int quantityOrdersPreviousYear;
    private int quantityOrdersPreviousYear0;
    private float ppiPrice;
    private float ppiValue;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date ppiDate;

    private String history;
    private int amountOfOrders;
    private int amountOfOrders0;
    private float orderQuantity;
    private float averageOrderQuantity;
    private int missingOrders;
    private float differenceToPreviousYear;
}
