package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

import java.util.List;

@Data
public class UserPage {
    private List<UserDto> pageUsers;
    private int userCount;
}
