package com.pelletsverein.backendpm.data.entitys;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "project")
public class ProjectEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(mappedBy = "projectEntity")
    private Set<ProjectSupplierEntity> projectSupplierEntities;

    private boolean active;
    private String shortDescription;
    private Date endOrderDate;
    private String path;
    private float expenses;
    private String loginPrefix;
    private int quantityPreviousYear;
    private int quantityOrdersPreviousYear;
    private int quantityOrdersPreviousYear0;
    private float ppiPrice;
    private float ppiValue;
    private Date ppiDate;
    private String history;
}
