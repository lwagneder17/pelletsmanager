package com.pelletsverein.backendpm.data.entitys;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "orders")
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "member_id")
    private MemberEntity memberEntity;

    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private ProjectSupplierEntity projectSupplierEntity;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @ManyToOne
    @JoinColumn(name = "ordertype_id")
    private OrderTypeEntity orderTypeEntity;

    @ManyToOne
    @JoinColumn(name = "contacttype_id")
    private ContactTypeEntity contactTypeEntity;

    private LocalDateTime orderDate;
    private int amountOrdered;
    private String wishedDelDate;
    private LocalDateTime memberConfirmation;
    private LocalDateTime supplierConfirmation;
    private String shortInfo;
    private String file;
}
