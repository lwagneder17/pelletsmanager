package com.pelletsverein.backendpm.data.dtos;

import lombok.Getter;

import java.util.Date;

@Getter
public class AddProjectDto {
    private String shortDescription;
    private Date endOrderDate;
}
