package com.pelletsverein.backendpm.data.database;

import com.pelletsverein.backendpm.data.entitys.SupplierEntity;
import org.springframework.data.repository.CrudRepository;

import javax.sql.rowset.CachedRowSet;

public interface SupplierRepository extends CrudRepository<SupplierEntity, Integer> {
}
