package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

@Data
public class EditUserDto {
    private String firstName;
    private String lastName;
    private String shortForm;
    private String password;
    private boolean active;
}
