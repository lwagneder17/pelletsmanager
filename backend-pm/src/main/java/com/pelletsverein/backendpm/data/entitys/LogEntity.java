package com.pelletsverein.backendpm.data.entitys;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "logging")
public class LogEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private ProjectEntity projectEntity;

    @ManyToOne
    @JoinColumn(name = "member_id")
    private MemberEntity memberEntity;

    @ManyToOne
    @JoinColumn(name = "orders_id")
    private OrderEntity orderEntity;

    @ManyToOne
    @JoinColumn(name = "message_type_id")
    private ContactTypeEntity contactTypeEntity;

    private LocalDateTime logDate;
    private String logType;
    private String messageStatus;
    private String messageReceiver;
}
