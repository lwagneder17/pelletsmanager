package com.pelletsverein.backendpm.data.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pelletsverein.backendpm.data.entitys.ContactTypeEntity;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class DetailedMemberDto {
    private int id;
    private ContactTypeEntity contactType;
    private List<ShortOrderDto> orders;

    @JsonFormat(pattern="dd.MM.yyyy HH:mm")
    private LocalDateTime regDate;

    private boolean active;
    private String title;
    private String additionalTitle;
    private String firstName;
    private String lastName;
    private String street;
    private String plz;
    private String city;
    private String privateNumber;
    private String secondaryNumber;
    private String email;
    private String delAddress;
    private String permanentInfo;
    private String internInfo;
    private String password;
    private String webId;
}
