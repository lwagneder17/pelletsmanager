package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class UserDto {
    private int id;
    private String firstName;
    private String lastName;
    private String shortForm;
    private String password;
    private boolean active;
    private Date creationDate;
}
