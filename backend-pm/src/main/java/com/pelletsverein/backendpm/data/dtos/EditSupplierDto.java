package com.pelletsverein.backendpm.data.dtos;

import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class EditSupplierDto {
    private String shortForm;
    private boolean active;
    private String companyName;
    private String contactName;
    private String greetingShort;
    private String greetingLong;
    private String street;
    private String plz;
    private String city;
    private String telephone;
    private String email;
    private String web;
    private String history;
}
