package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;
import lombok.Setter;

@Data
public class OrderTypeStatsDto {
    private String contactType;
    private int amountOfOrders;
    private double shareOfOverallOrderQuantity;
}
