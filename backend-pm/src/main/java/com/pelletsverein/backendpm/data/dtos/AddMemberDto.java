package com.pelletsverein.backendpm.data.dtos;

import lombok.Getter;

@Getter
public class AddMemberDto {
    private String title;
    private String additionalTitle;
    private String firstName;
    private String lastName;
    private String email;
    private int contactTypeId;
    private String plz;
    private String city;
    private String street;
    private String deliveryAddress;
    private String privateNumber;
    private String secondaryNumber;
    private String internInfo;
    private String permanentInfo;
}
