package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

@Data
public class ShortProjectSupplierDto {
    private int id;
    private String supplierShortForm;
    private String supplierCompanyName;
    private float pelletsPrice;
    private float alpPrice;
    private String deliveryEnd;
}
