package com.pelletsverein.backendpm.data.database;

import com.pelletsverein.backendpm.data.entitys.LogEntity;
import org.springframework.data.repository.CrudRepository;

public interface LoggingRepository extends CrudRepository<LogEntity, Integer> {
}