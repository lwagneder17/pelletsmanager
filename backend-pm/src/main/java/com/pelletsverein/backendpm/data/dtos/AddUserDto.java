package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

@Data
public class AddUserDto {
    private String lastName;
    private String firstName;
    private String password;
}
