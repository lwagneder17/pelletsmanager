package com.pelletsverein.backendpm.data.classes;

import com.pelletsverein.backendpm.data.entitys.OrderEntity;
import com.pelletsverein.backendpm.data.entitys.ProjectEntity;
import com.pelletsverein.backendpm.data.entitys.SupplierEntity;
import lombok.Data;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

@Data
public class ProjectSupplier {
    private int supplierId;
    private int projectId;
    private float pelletsPrice;
    private float alpPrice;
    private Date deliveryEnd;
    private float minDeliveryAmount;
    private int amountOfOrders;
    private float orderQuantity;
}
