package com.pelletsverein.backendpm.data.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pelletsverein.backendpm.data.entitys.*;
import lombok.Data;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class DetailedOrderDto {
    private int id;
    private ShortMemberDto member;
    private ShortProjectSupplierDto projectSupplier;
    private OrderTypeEntity orderType;
    private ContactTypeEntity contactType;
    private List<String> availableDeliveryDateOptions;

    @JsonFormat(pattern="dd.MM.yyyy HH:mm")
    private LocalDateTime orderDate;

    private int amountOrdered;
    private String shortInfo;
}
