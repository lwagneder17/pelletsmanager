package com.pelletsverein.backendpm.data.dtos;

import lombok.Data;

@Data
public class ShortOrderDto {
    private int id;
    private ShortMemberDto member;
    private String supplierShortForm;
    private String username;
    private String contactType;
    private String wishedDelDate;
    private int amountOrdered;
    private boolean memberConfirmation;
    private boolean supplierConfirmation;
    private String file;
}
