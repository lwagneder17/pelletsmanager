package com.pelletsverein.backendpm.service;

import com.pelletsverein.backendpm.data.classes.*;
import com.pelletsverein.backendpm.data.dtos.*;
import com.pelletsverein.backendpm.data.entitys.*;
import com.pelletsverein.backendpm.manager.DataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;

@Component
public class DataService {

    @Autowired
    private DataManager dataManager;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private DecimalFormat df = new DecimalFormat("#.##");

    private List<MemberEntity> currentMembers;

    //region Login
    public UserDto authenticateLogin(LoginDto loginDto) {
        UserEntity user = dataManager.authenticateLogin(loginDto);
        if(user == null){
            return null;
        }
        else{
            return convertUserEntityToUserDto(user);
        }

    }

    private UserDto convertUserEntityToUserDto(UserEntity user) {
        UserDto userDto = new UserDto();

        userDto.setId(user.getId());
        userDto.setCreationDate(user.getCreationDate());
        userDto.setActive(user.isActive());
        userDto.setPassword(user.getPassword());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setShortForm(user.getShortForm());

        return userDto;
    }

    //endregion

    //region Member
    public MemberPage getMembers(int pageSize, int pageNr, int filterId, int searchId, String searchString) {
        MemberPage memberPage = new MemberPage();
        currentMembers = getMembersByFilterOption(filterId);

        if(searchString.isEmpty()){
            memberPage.setMemberCount(currentMembers.size());
            memberPage.setPageMembers(currentMembers.stream()
                    .skip((pageNr-1)*pageSize)
                    .limit(pageSize)
                    .map(x -> convertMemberEntityToShortDto(x))
                    .toList());

            return memberPage;
        }
        switch(searchId) {
            case 0:
                return getMembersByDefaultSearch(searchString, pageSize, pageNr);
            case 1:
                return getMembersByNameSearch(searchString, pageSize, pageNr);

            default:
                return null;
        }
    }

    public List<ShortOrderDto> getOrdersOfMember(int memberId) {
        return dataManager.getMemberById(memberId).getOrders().stream().map(x -> convertOrderEntityToShortDto(x)).toList();
    }

    private List<MemberEntity> getMembersByFilterOption(int filterOptionId) {
        switch(filterOptionId){
            case 0:
                return dataManager.getMembers();

            case 1:
                return getActiveMembers();

            case 2:
                return getInactiveMembers();

            case 3:
                return getMembersWithOrderAmount0(2);

            case 4:
                return getMembersWithOrderAmount0(3);

            case 5:
                return getMembersByContactType("Mail");

            case 6:
                return getMembersByContactType("Brief");

            case 7:
                return getMembersWithNoOrdersInCurrentProject();

            default:
                return null;
        }
    }

    public List<String> getMemberFilterOptions() {
        return List.of("Alle", "Aktive", "Nicht aktive", "2 Projekte Bestellmenge=0", "3 Projekte Bestellmenge=0", "Formular per Mail", "Formular per Brief", "Bestelltyp - Fehlende");
    }

    private List<MemberEntity> getMembersByContactType(String contactType) {
        ContactTypeEntity contactTypeEntity = dataManager.getContactTypes().stream().filter(x -> x.getType().equals(contactType)).findFirst().get();
        return dataManager.getMembers().stream().filter(x -> x.getContactTypeEntity().equals(contactTypeEntity)).toList();
    }

    private List<MemberEntity> getMembersWithOrderAmount0(int amountOfProjects) {
        Map<Integer, List<MemberEntity>> memberMap = new HashMap();
        currentMembers = new ArrayList<>();
        List<ProjectEntity> projects = dataManager.getProjects();

        if(projects.size() >= amountOfProjects + 1){
            for(int i = 1; i <= amountOfProjects; i++){
                ProjectEntity project = projects.get(projects.size()-1-i);
                List<ProjectSupplierEntity> projectSuppliers = project.getProjectSupplierEntities().stream().toList();

                for(MemberEntity member : dataManager.getMembers()){
                    List<OrderEntity> ordersWithOrderAmountInCurrentProject = new ArrayList<>();
                    for(OrderEntity order : member.getOrders()){
                        if(projectSuppliers.contains(order.getProjectSupplierEntity()) && order.getAmountOrdered() != 0){
                            ordersWithOrderAmountInCurrentProject.add(order);
                        }
                    }
                    if(ordersWithOrderAmountInCurrentProject.size() == 0){
                        if(memberMap.containsKey(member.getId())){
                            memberMap.get(member.getId()).add(member);
                        }
                        else{
                            List<MemberEntity> memberList = new ArrayList<>();
                            memberList.add(member);
                            memberMap.put(member.getId(), memberList);
                        }
                    }
                }
            }
        }

        memberMap.keySet().stream().forEach(x -> {
            if(memberMap.get(x).size() == amountOfProjects) currentMembers.add(dataManager.getMemberById(x));
        });

        return currentMembers.stream()
                .sorted((x, y) -> x.getId() - y.getId())
                .toList();
    }

    private List<MemberEntity> getMembersWithNoOrdersInCurrentProject() {
        ProjectEntity project = dataManager.getProjects().get(dataManager.getProjects().size()-1);

        List<ProjectSupplierEntity> projectSuppliers = project.getProjectSupplierEntities().stream().toList();

        return (dataManager.getMembers().stream()
                .filter(x -> x.getOrders().stream()
                        .filter(y -> projectSuppliers.contains(y.getProjectSupplierEntity())).count() == 0).toList());
    }

    private List<MemberEntity> getActiveMembers() {
        return dataManager.getMembers().stream().filter(x -> x.isActive()).toList();
    }

    private List<MemberEntity> getInactiveMembers() {
        return dataManager.getMembers().stream().filter(x -> x.isActive() == false).toList();
    }
    public MemberPage getMembersByDefaultSearch(String searchString, int pageSize, int pageNr) {
        String editedSearchString = searchString.trim().replace(" ", "").toLowerCase();

        List<ShortMemberDto> membersFound = currentMembers.stream().filter(x -> {
            if(x.getFirstName().toLowerCase().contains(editedSearchString)){
                return true;
            }
            else if(x.getLastName().toLowerCase().contains(editedSearchString)){
                return true;
            }
            else if(x.getPrivateNumber().toLowerCase().contains(editedSearchString)){
                return true;
            }
            else if(x.getSecondaryNumber().toLowerCase().contains(editedSearchString)){
                return true;
            }
            else if(x.getEmail().toLowerCase().contains(editedSearchString)){
                return true;
            }
            else if(x.getCity().toLowerCase().contains(editedSearchString)){
                return true;
            }

            return false;

        }).map(x -> convertMemberEntityToShortDto(x)).toList();

        MemberPage memberPage = new MemberPage();

        memberPage.setMemberCount(membersFound.size());
        memberPage.setPageMembers(membersFound.stream()
                .skip((pageNr-1)*pageSize)
                .limit(pageSize)
                .toList());

        return memberPage;
    }

    public MemberPage getMembersByNameSearch(String searchString, int pageSize, int pageNr) {
        String editedSearchString = searchString.trim().replace(" ", "").toLowerCase();

        List<ShortMemberDto> membersFound = currentMembers.stream()
                .filter(x -> {
                    String shortForm = x.getLastName().substring(0, 2) + x.getFirstName().substring(0, 2);
                    return shortForm.toLowerCase().contains(editedSearchString);
                })
                .map(x -> convertMemberEntityToShortDto(x)).toList();

        MemberPage memberPage = new MemberPage();

        memberPage.setMemberCount(membersFound.size());
        memberPage.setPageMembers(membersFound.stream()
                .skip((pageNr-1)*pageSize)
                .limit(pageSize)
                .toList());

        return memberPage;
    }

    public DetailedMemberDto getMember(int id) {
        DetailedMemberDto detailedMemberDto = convertMemberEntityToDetailedMemberDto(dataManager.getMemberById(id));
        return detailedMemberDto;
    }

    public ShortMemberDto addNewMember(AddMemberDto addMemberDto) {
        Member member = convertAddMemberDtoToMember(addMemberDto);
        return convertMemberEntityToShortDto(dataManager.addMember(member));
    }

    public DetailedMemberDto editMember(int memberId, EditMemberDto memberDto) {
        Member member = convertEditMemberDtoToMember(memberId, memberDto);
        return convertMemberEntityToDetailedMemberDto(dataManager.editMember(member));
    }

    public String deleteMember(int memberId) {
        return dataManager.deleteMember(memberId);
    }
    //endregion

    //region Order
    public OrderPage getOrders(int pageSize, int pageNr, String searchString) {
        OrderPage orderPage = new OrderPage();
        List<OrderEntity> orders;

        if(!searchString.isBlank()){
            orders = dataManager.getOrders().stream()
                    .filter(x -> {
                        String memberName = x.getMemberEntity().getLastName() + " " + x.getMemberEntity().getFirstName();
                        if(memberName.toLowerCase().contains(searchString.toLowerCase())){
                            return true;
                        }
                        else if(x.getMemberEntity().getStreet().toLowerCase().contains(searchString.toLowerCase())){
                            return true;
                        }
                        else if(x.getMemberEntity().getCity().toLowerCase().contains(searchString.toLowerCase())){
                            return true;
                        }
                        else if(x.getMemberEntity().getPlz().equalsIgnoreCase(searchString)){
                            return true;
                        }
                        else if(x.getProjectSupplierEntity().getSupplierEntity().getShortForm().equalsIgnoreCase(searchString)){
                            return true;
                        }
                        else{
                            return false;
                        }
                    })
                    .toList();
        }
        else{
            orders = dataManager.getOrders();
        }

        orderPage.setPageOrders(orders.stream()
                .skip(pageSize*(pageNr-1))
                .limit(pageSize)
                .map(x -> convertOrderEntityToShortDto(x))
                .toList());
        orderPage.setOrderCount(orders.size());

        return orderPage;
    }

    public DetailedOrderDto getOrder(int orderId) {
        return convertOrderEntityToDetailedDto(dataManager.getOrderById(orderId));
    }

    public ShortOrderDto addNewOrder(AddOrderDto addOrderDto) {
        Order order = convertAddOrderDtoToOrder(addOrderDto);
        ProjectSupplierEntity supplier = dataManager.getProjectSupplierById(order.getProjectSupplierId());
        supplier.setOrderQuantity(supplier.getOrderQuantity() + order.getAmountOrdered());
        supplier.setAmountOfOrders(supplier.getAmountOfOrders() + 1);
        return convertOrderEntityToShortDto(dataManager.addOrder(order));
    }

    public DetailedOrderDto editOrder(int orderId, EditOrderDto editOrderDto) {
        Order order = convertEditOrderDtoToOrder(editOrderDto);
        return convertOrderEntityToDetailedDto(dataManager.editOrder(orderId, order));
    }

    public String deleteOrder(int orderId) {
        return dataManager.deleteOrder(orderId);
    }
    //endregion

    //region Supplier
    public SupplierPage getSuppliers(int pageSize, int pageNr) {
        SupplierPage supplierPage = new SupplierPage();
        List<SupplierEntity> suppliers;

        suppliers = dataManager.getSuppliers();
        supplierPage.setPageSuppliers(suppliers.stream()
                .skip(pageSize*(pageNr-1))
                .limit(pageSize)
                .map(x -> convertSupplierEntityToDto(x))
                .toList());
        supplierPage.setSupplierCount(suppliers.size());

        return supplierPage;
    }

    public SupplierDto getSupplier(int supplierId) {
        return convertSupplierEntityToDto(dataManager.getSupplierById(supplierId));
    }

    public SupplierDto addSupplier(AddSupplierDto addSupplierDto) {
        Supplier supplier = convertAddSupplierDtoToSupplier(addSupplierDto);
        supplier.setRegDate(LocalDateTime.now());
        return convertSupplierEntityToDto(dataManager.addSupplier(supplier));
    }

    public SupplierDto editSupplier(int supplierId, EditSupplierDto addSupplierDto) {
        Supplier supplier = convertEditSupplierDtoToSupplier(addSupplierDto);
        return convertSupplierEntityToDto(dataManager.editSupplier(supplierId, supplier));
    }

    public String deleteSupplier(int supplierId) {
        return dataManager.deleteSupplier(supplierId);
    }
    //endregion

    //region Project
    public ProjectPage getProjects(int pageSize, int pageNr) {
        List<ProjectEntity> projects;
        ProjectPage projectPage = new ProjectPage();

        projects = dataManager.getProjects();
        projectPage.setPageProjects(projects.stream()
                .skip(pageSize*(pageNr-1))
                .limit(pageSize)
                .map(x -> convertProjectEntityToShortDto(x))
                .toList());
        projectPage.setProjectCount(projects.size());

        return projectPage;
    }

    public DetailedProjectDto getProject(int projectId) {
        return convertProjectEntityToDetailedDto(dataManager.getProjectById(projectId));
    }

    public ShortProjectDto addProjects(AddProjectDto addProjectDto) {
        Project project = convertAddProjectDtoToProject(addProjectDto);
        return convertProjectEntityToShortDto(dataManager.addProject(project));
    }

    public DetailedProjectDto editProject(int projectId, EditProjectDto editProjectDto) {
        Project project = convertEditProjectDtoToProject(editProjectDto);
        return convertProjectEntityToDetailedDto(dataManager.editProject(projectId, project));
    }

    public String deleteProject(int projectId) {
        return dataManager.deleteProject(projectId);
    }

    public List<SupplierDto> getAvailableSuppliersForProject(int projectId) {
        List<Integer> projectSupplierIds = dataManager.getProjectById(projectId)
                .getProjectSupplierEntities()
                .stream()
                .map(x -> x.getSupplierEntity().getId())
                .toList();

        List<SupplierEntity> activeSuppliers = dataManager.getSuppliers().stream().filter(x -> x.isActive()).toList();

        return activeSuppliers
                .stream()
                .filter(x -> !projectSupplierIds.contains(x.getId()))
                .map(x -> convertSupplierEntityToDto(x))
                .toList();
    }

    private double[] getProjectOrderStats(int projectId){
        ProjectEntity project = dataManager.getProjectById(projectId);
        double amountOfOrdersWithQuantity = 0;
        double amountOfOrders0 = 0;
        double quantityOfOrders = 0;
        double averageOrderQuantity = 0;
        double missingOrders = 0;
        double missingOrdersPreviousYear = 0;
        double missingOrdersDifferenceToPreviousYear = 0;

        for (ProjectSupplierEntity supplier : project.getProjectSupplierEntities()) {
            for (OrderEntity order : supplier.getOrders()) {
                if(order.getAmountOrdered() == 0){
                    amountOfOrders0 += 1;
                }
                else{
                    amountOfOrdersWithQuantity += 1;
                }
                quantityOfOrders += order.getAmountOrdered();
            }
        }

        for(MemberEntity member : dataManager.getMembers().stream().filter(x -> x.isActive()).toList()){
            int ordersInCurrentProject = 0;
            for (OrderEntity order : member.getOrders()) {
                if(project.getProjectSupplierEntities().contains(order.getProjectSupplierEntity())){
                    ordersInCurrentProject++;
                    break;
                }
            }
            if(ordersInCurrentProject == 0) missingOrders += 1;
        }

        if(dataManager.getProjects().size() > 1){
            for(MemberEntity member : dataManager.getMembers().stream().filter(x -> x.isActive()).toList()){
                int ordersInCurrentProject = 0;
                for (OrderEntity order : member.getOrders()) {
                    if(dataManager.getProjects().get(dataManager.getProjects().indexOf(project)-1).getProjectSupplierEntities().contains(order.getProjectSupplierEntity())){
                        ordersInCurrentProject += 1;
                        break;
                    }
                }
                if(ordersInCurrentProject == 0) missingOrdersPreviousYear += 1;
            }
        }

        if(quantityOfOrders > 0) averageOrderQuantity = quantityOfOrders / (amountOfOrdersWithQuantity + amountOfOrders0);
        if(missingOrdersPreviousYear > 0) missingOrdersDifferenceToPreviousYear = (missingOrders/missingOrdersPreviousYear)*100-100;
        return new double[]{amountOfOrdersWithQuantity, amountOfOrders0, quantityOfOrders, averageOrderQuantity, missingOrders, missingOrdersDifferenceToPreviousYear};
    }

    public List<ProjectSupplierStatsDto> getProjectSupplierStats(int projectId){
        List<ProjectSupplierStatsDto> projectSupplierStatsDtos = new ArrayList<>();
        double quantityOfOrdersCurrentProject = 0;

        for (ProjectSupplierEntity supplier : dataManager.getProjectById(projectId).getProjectSupplierEntities()) {
            for (OrderEntity order : supplier.getOrders()) {
                quantityOfOrdersCurrentProject += order.getAmountOrdered();
            }
        }
        for (ProjectSupplierEntity supplier : dataManager.getProjectById(projectId).getProjectSupplierEntities()) {
            double amountOfOrders = 0;
            double orderQuantity = 0;
            double shareOfOverallOrderQuantity = 0;

            for (OrderEntity order : supplier.getOrders()) {
                amountOfOrders++;
                orderQuantity += order.getAmountOrdered();
            }

            ProjectSupplierStatsDto supplierStats = new ProjectSupplierStatsDto();

            supplierStats.setSupplierName(supplier.getSupplierEntity().getCompanyName());
            supplierStats.setAmountOfOrders((int)amountOfOrders);
            supplierStats.setOrderQuantity(orderQuantity);
            if(amountOfOrders != 0) shareOfOverallOrderQuantity = (orderQuantity/quantityOfOrdersCurrentProject)*100;
            supplierStats.setShareOfOverallOrderQuantity(shareOfOverallOrderQuantity);

            projectSupplierStatsDtos.add(supplierStats);
        }
        return projectSupplierStatsDtos;
    }

    public List<ContactTypeStatsDto> getContactTypeStats(int projectId){
        Map<String, Integer> contactTypeStats = new HashMap<>();
        List<ContactTypeStatsDto> contactTypeStatsDtos = new ArrayList<>();
        double quantityOfOrders = 0;
        double shareOfOverallOrderQuantity = 0;

        for (ProjectSupplierEntity supplier : dataManager.getProjectById(projectId).getProjectSupplierEntities()) {
            for (OrderEntity order : supplier.getOrders()) {
                quantityOfOrders += order.getAmountOrdered();
                if(contactTypeStats.containsKey(order.getContactTypeEntity().getType())){
                    contactTypeStats.put(order.getContactTypeEntity().getType(), contactTypeStats.get(order.getContactTypeEntity().getType() + order.getAmountOrdered()));
                }
                else{
                    contactTypeStats.put(order.getContactTypeEntity().getType(), order.getAmountOrdered());
                }
            }
        }
        for (String key : contactTypeStats.keySet()) {
            ContactTypeStatsDto contactTypeStatsDto = new ContactTypeStatsDto();

            contactTypeStatsDto.setContactType(key);
            contactTypeStatsDto.setAmountOfOrders(contactTypeStats.get(key));
            if(contactTypeStats.get(key) != 0) shareOfOverallOrderQuantity = (contactTypeStats.get(key)/quantityOfOrders)*100;
            contactTypeStatsDto.setShareOfOverallOrderQuantity(shareOfOverallOrderQuantity);

            contactTypeStatsDtos.add(contactTypeStatsDto);
        }
        return contactTypeStatsDtos;
    }

    public List<OrderTypeStatsDto> getOrderTypeStats(int projectId){
        Map<String, Integer> orderTypeStats = new HashMap<>();
        List<OrderTypeStatsDto> orderTypeStatsDtos = new ArrayList<>();
        double quantityOfOrders = 0;
        double shareOfOverallOrderQuantity = 0;

        for (ProjectSupplierEntity supplier : dataManager.getProjectById(projectId).getProjectSupplierEntities()) {
            for (OrderEntity order : supplier.getOrders()) {
                quantityOfOrders += order.getAmountOrdered();
                if(orderTypeStats.containsKey(order.getOrderTypeEntity().getType())){
                    orderTypeStats.put(order.getOrderTypeEntity().getType(), orderTypeStats.get(order.getOrderTypeEntity().getType() + order.getAmountOrdered()));
                }
                else{
                    orderTypeStats.put(order.getOrderTypeEntity().getType(), order.getAmountOrdered());
                }
            }
        }
        for (String key : orderTypeStats.keySet()) {
            OrderTypeStatsDto orderTypeStatsDto = new OrderTypeStatsDto();

            orderTypeStatsDto.setContactType(key);
            orderTypeStatsDto.setAmountOfOrders(orderTypeStats.get(key));
            if(orderTypeStats.get(key) != 0) shareOfOverallOrderQuantity = (orderTypeStats.get(key)/quantityOfOrders)*100;
            orderTypeStatsDto.setShareOfOverallOrderQuantity(shareOfOverallOrderQuantity);

            orderTypeStatsDtos.add(orderTypeStatsDto);
        }
        return orderTypeStatsDtos;
    }
    //endregion


    //region ProjectSupplier
    public List<ShortProjectSupplierDto> getProjectSuppliers() {
        return dataManager.getProjectSuppliers().stream().map(x -> convertProjectSupplierToShortDto(x)).toList();
    }

    public List<String> getSupplierFilterOptions() {
        return List.of("Alle", "Aktive", "Nicht aktive");
    }

    public DetailedProjectSupplierDto getProjectSupplier(int projectSupplierId) {
        return convertProjectSupplierToDetailedDto(dataManager.getProjectSupplierById(projectSupplierId));
    }

    public DetailedProjectSupplierDto addProjectSupplier(AddProjectSupplierDto addProjectSupplierDto) {
        ProjectSupplier projectSupplier = convertAddProjectSupplierDtoToProjectSupplier(addProjectSupplierDto);
        return convertProjectSupplierToDetailedDto(dataManager.addProjectSupplier(projectSupplier));
    }

    public DetailedProjectSupplierDto editProjectSupplier(int projectSupplierId, EditProjectSupplierDto editProjectSupplierDto) {
        ProjectSupplier projectSupplier = convertEditProjectSupplierDtoToProjectSupplier(editProjectSupplierDto);
        return convertProjectSupplierToDetailedDto(dataManager.editProjectSupplier(projectSupplierId, projectSupplier));
    }

    public String deleteProjectSupplier(int projectSupplierId) {
        return dataManager.deleteProjectSupplier(projectSupplierId);
    }
    //endregion

    //region User
    public UserPage getUsers(int pageSize, int pageNr) {
        UserPage userPage = new UserPage();
        List<UserEntity> users = dataManager.getUsers();

        userPage.setPageUsers(users.stream()
                .skip(pageSize*(pageNr-1))
                .limit(pageSize)
                .map(x -> convertUserEntityToUserDto(x))
                .toList());
        userPage.setUserCount(users.size());

        return userPage;
    }

    public void addUser(AddUserDto newUser){
        dataManager.addUser(convertAddUserToUser(newUser));
    }

    public void deleteUser(int userId){
        dataManager.deleteUser(userId);
    }

    public UserDto editUser(int userId, EditUserDto editUser) {
        return convertUserEntityToUserDto(dataManager.editUser(userId, convertEditUserToUser(editUser)));
    }
    //endregion

    //region Helpers
    public List<ContactTypeEntity> getContactTypes() {
        return dataManager.getContactTypes();
    }

    private User convertAddUserToUser(AddUserDto newUser) {
        User user = new User();

        user.setLastName(newUser.getLastName());
        user.setFirstName(newUser.getFirstName());
        user.setPassword(newUser.getPassword());
        user.setActive(true);
        user.setCreationDate(new Date());
        user.setShortForm(newUser.getLastName().substring(0, 2) + newUser.getFirstName().substring(0, 2));

        return user;
    }

    private User convertEditUserToUser(EditUserDto editUser) {
        User user = new User();

        user.setLastName(editUser.getLastName());
        user.setFirstName(editUser.getFirstName());
        user.setPassword(editUser.getPassword());
        user.setActive(editUser.isActive());
        user.setShortForm(editUser.getShortForm());

        return user;
    }

    public List<OrderTypeEntity> getOrderTypes() {
        return dataManager.getOrderTypes();
    }

    private List<SupplierEntity> getActiveSuppliers() {
        return dataManager.getSuppliers().stream().filter(x -> x.isActive()).toList();
    }

    private List<SupplierEntity> getInactiveSuppliers() {
        return dataManager.getSuppliers().stream().filter(x -> x.isActive() == false).toList();
    }

    public List<SupplierDto> getSuppliersByDefaultSearch(String searchString) {
        if(searchString == null){
            return dataManager.getSuppliers().stream().map(x -> convertSupplierEntityToDto(x)).toList();
        }

        String editedSearchString = searchString.trim().toLowerCase();
        return dataManager.getSuppliers().stream().filter(x -> {
            if((x.getCompanyName()).toLowerCase().contains(editedSearchString)){
                return true;
            }
            else if(x.getId() == Integer.parseInt(editedSearchString)){
                return true;
            }
            else if(x.getShortForm().toLowerCase().contains(editedSearchString)){
                return true;
            }

            return false;

        }).map(x -> convertSupplierEntityToDto(x)).toList();
    }

    public List<SupplierDto> getSuppliersByFilterOption(int filterOptionId) {
        switch(filterOptionId){
            case 0: return dataManager.getSuppliers().stream().map(x -> convertSupplierEntityToDto(x)).toList();
            case 1: return getActiveSuppliers().stream().map(x -> convertSupplierEntityToDto(x)).toList();
            case 2: return getInactiveSuppliers().stream().map(x -> convertSupplierEntityToDto(x)).toList();

            default: return null;
        }
    }

    public List<ShortOrderDto> getOrdersByDefaultSearch(String searchString) {
        if(searchString == null){
            return dataManager.getOrders().stream().map(x -> convertOrderEntityToShortDto(x)).toList();
        }

        String editedSearchString = searchString.trim().toLowerCase();
        return dataManager.getOrders().stream().filter(x -> {
            try {
                if((x.getMemberEntity().getFirstName() +" "+ x.getMemberEntity().getLastName()).toLowerCase().contains(editedSearchString)){
                    return true;
                }
                else if(x.getContactTypeEntity().getType().toLowerCase().contains(editedSearchString)){
                    return true;
                }
                else if(x.getWishedDelDate().toLowerCase().contains(editedSearchString)){
                    return true;
                }
                else if(x.getId() == Integer.parseInt(editedSearchString)){
                    return true;
                }
            }
            catch (NumberFormatException formatException){}
            return false;

        }).map(x -> convertOrderEntityToShortDto(x)).toList();
    }

    public List<String> getAvailableDeliveryDates(int projectSupplierId) {
        List<String> availableDeliveryDates = new ArrayList<>(Arrays.asList("Dringend", "Jederzeit", "Kein Bedarf"));
        LocalDate currentDate = LocalDate.now();
        LocalDate deliveryEndDate = new java.sql.Date(dataManager.getProjectSupplierById(projectSupplierId).getDeliveryEnd().getTime()).toLocalDate();

        while((currentDate.getMonth().getValue() <= deliveryEndDate.getMonth().getValue() && currentDate.getYear() == deliveryEndDate.getYear()) || currentDate.getYear() < deliveryEndDate.getYear()){
            String month = currentDate.getMonth().getDisplayName(TextStyle.FULL, Locale.GERMAN);
            String year = String.valueOf(currentDate.getYear());
            availableDeliveryDates.add(month + " " + year);
            currentDate = currentDate.plusMonths(1);
        }
        return availableDeliveryDates;
    }

    private ProjectSupplier convertEditProjectSupplierDtoToProjectSupplier(EditProjectSupplierDto editProjectSupplierDto) {
        ProjectSupplier projectSupplier = new ProjectSupplier();

        projectSupplier.setSupplierId(editProjectSupplierDto.getSupplierId());
        projectSupplier.setProjectId(editProjectSupplierDto.getProjectId());
        projectSupplier.setPelletsPrice(editProjectSupplierDto.getPelletsPrice());
        projectSupplier.setAlpPrice(editProjectSupplierDto.getAlpPrice());
        if(editProjectSupplierDto.getDeliveryEnd() != null) projectSupplier.setDeliveryEnd(editProjectSupplierDto.getDeliveryEnd());
        projectSupplier.setMinDeliveryAmount(editProjectSupplierDto.getMinDeliveryAmount());
        projectSupplier.setAmountOfOrders(editProjectSupplierDto.getAmountOfOrders());
        projectSupplier.setOrderQuantity(editProjectSupplierDto.getOrderQuantity());

        return projectSupplier;
    }
    private ProjectSupplier convertAddProjectSupplierDtoToProjectSupplier(AddProjectSupplierDto addProjectSupplierDto) {
        ProjectSupplier projectSupplier = new ProjectSupplier();

        projectSupplier.setSupplierId(addProjectSupplierDto.getSupplierId());
        projectSupplier.setProjectId(addProjectSupplierDto.getProjectId());
        projectSupplier.setPelletsPrice(addProjectSupplierDto.getPelletsPrice());
        projectSupplier.setAlpPrice(addProjectSupplierDto.getAlpPrice());
        projectSupplier.setDeliveryEnd(addProjectSupplierDto.getEndOrderDate());
        projectSupplier.setMinDeliveryAmount(addProjectSupplierDto.getMinOrderAmount());
        projectSupplier.setAmountOfOrders(0);
        projectSupplier.setOrderQuantity(0);

        return projectSupplier;
    }
    private DetailedProjectSupplierDto convertProjectSupplierToDetailedDto(ProjectSupplierEntity projectSupplier) {
        DetailedProjectSupplierDto supplierDto = new DetailedProjectSupplierDto();

        supplierDto.setId(projectSupplier.getId());

        SupplierEntity supplier = projectSupplier.getSupplierEntity();
        supplierDto.setSupplierId(supplier.getId());
        supplierDto.setContactName(supplier.getContactName());
        supplierDto.setSupplierName(supplier.getCompanyName());
        supplierDto.setSupplierShortForm(supplier.getShortForm());
        supplierDto.setTelephoneNumber(supplier.getTelephone());
        supplierDto.setEmail(supplier.getEmail());

        supplierDto.setPelletsPrice(projectSupplier.getPelletsPrice());
        supplierDto.setAlpPrice(projectSupplier.getAlpPrice());
        supplierDto.setDeliveryEnd(projectSupplier.getDeliveryEnd());
        supplierDto.setMinDeliveryAmount(projectSupplier.getMinDeliveryAmount());
        supplierDto.setAmountOfOrders(projectSupplier.getAmountOfOrders());
        supplierDto.setOrderQuantity(projectSupplier.getOrderQuantity());
        if(supplierDto.getAmountOfOrders() > 0 && supplierDto.getOrderQuantity() > 0){
            supplierDto.setAverageOrderQuantity(projectSupplier.getOrderQuantity() / projectSupplier.getAmountOfOrders());
        }
        else{
            supplierDto.setAverageOrderQuantity(0);
        }

        return supplierDto;
    }
    private Project convertEditProjectDtoToProject(EditProjectDto editProjectDto) {
        Project project = new Project();

        project.setActive(editProjectDto.isActive());
        project.setShortDescription(editProjectDto.getShortDescription());
        if(editProjectDto.getEndOrderDate() != null) project.setEndOrderDate(editProjectDto.getEndOrderDate());
        project.setPath(editProjectDto.getPath());
        project.setExpenses(editProjectDto.getExpenses());
        project.setLoginPrefix(editProjectDto.getLoginPrefix());
        project.setQuantityPreviousYear(editProjectDto.getQuantityPreviousYear());
        project.setQuantityOrdersPreviousYear(editProjectDto.getQuantityOrdersPreviousYear());
        project.setQuantityOrdersPreviousYear0(editProjectDto.getQuantityOrdersPreviousYear0());
        project.setPpiPrice(editProjectDto.getPpiPrice());
        project.setPpiValue(editProjectDto.getPpiValue());
        if(editProjectDto.getPpiDate() != null) project.setPpiDate(editProjectDto.getPpiDate());
        project.setHistory(editProjectDto.getHistory());

        return project;
    }
    private Project convertAddProjectDtoToProject(AddProjectDto addProjectDto) {
        Project project = new Project();

        project.setActive(true);
        project.setShortDescription(addProjectDto.getShortDescription());
        if(addProjectDto.getEndOrderDate() != null) project.setEndOrderDate(addProjectDto.getEndOrderDate());
        project.setPath("2023");
        project.setExpenses(0);
        project.setLoginPrefix("pk");
        project.setQuantityPreviousYear(0);
        project.setQuantityOrdersPreviousYear(0);
        project.setQuantityOrdersPreviousYear0(0);
        project.setPpiPrice(0);
        project.setPpiValue(0);
        project.setPpiDate(null);
        project.setHistory(null);

        return project;
    }
    private DetailedProjectDto convertProjectEntityToDetailedDto(ProjectEntity projectEntity){
        DetailedProjectDto detailedProjectDto = new DetailedProjectDto();

        detailedProjectDto.setId(projectEntity.getId());
        detailedProjectDto.setProjectSuppliers(projectEntity.getProjectSupplierEntities().stream().map(x -> convertProjectSupplierToDetailedDto(x)).toList());
        detailedProjectDto.setActive(projectEntity.isActive());
        detailedProjectDto.setShortDescription(projectEntity.getShortDescription());
        detailedProjectDto.setEndOrderDate(projectEntity.getEndOrderDate());
        detailedProjectDto.setPath(projectEntity.getPath());
        detailedProjectDto.setExpenses(projectEntity.getExpenses());
        detailedProjectDto.setLoginPrefix(projectEntity.getLoginPrefix());
        detailedProjectDto.setQuantityPreviousYear(projectEntity.getQuantityPreviousYear());
        detailedProjectDto.setQuantityOrdersPreviousYear(projectEntity.getQuantityOrdersPreviousYear());
        detailedProjectDto.setQuantityOrdersPreviousYear0(projectEntity.getQuantityOrdersPreviousYear0());
        detailedProjectDto.setPpiPrice(projectEntity.getPpiPrice());
        detailedProjectDto.setPpiValue(projectEntity.getPpiValue());
        detailedProjectDto.setPpiDate(projectEntity.getPpiDate());
        detailedProjectDto.setHistory(projectEntity.getHistory());

        double[] projectStats = getProjectOrderStats(projectEntity.getId());
        detailedProjectDto.setAmountOfOrders((int)projectStats[0]);
        detailedProjectDto.setAmountOfOrders0((int)projectStats[1]);
        detailedProjectDto.setOrderQuantity((float)projectStats[2]);
        detailedProjectDto.setAverageOrderQuantity((float)projectStats[3]);
        detailedProjectDto.setMissingOrders((int)projectStats[4]);
        detailedProjectDto.setDifferenceToPreviousYear((float)projectStats[5]);

        return detailedProjectDto;
    }
    private ShortProjectDto convertProjectEntityToShortDto(ProjectEntity projectEntity) {
        ShortProjectDto shortProjectDto = new ShortProjectDto();

        shortProjectDto.setId(projectEntity.getId());
        shortProjectDto.setActive(projectEntity.isActive());
        shortProjectDto.setProjectName(projectEntity.getShortDescription());
        shortProjectDto.setSupplierShortCodes(getSupplierShortCodes(projectEntity.getProjectSupplierEntities()));
        double[] projectOrderStats = calculateProjectStats(projectEntity.getProjectSupplierEntities());
        shortProjectDto.setOrderQuantity(projectOrderStats[0]);
        shortProjectDto.setAmountOfOrders(projectOrderStats[1]);
        if(projectOrderStats[0] != 0 && projectOrderStats[1] != 0)shortProjectDto.setAverageOrderQuantity(projectOrderStats[0]/projectOrderStats[1]);
        shortProjectDto.setPpiPrice(projectEntity.getPpiPrice());
        shortProjectDto.setPpiValue(projectEntity.getPpiValue());

        return shortProjectDto;
    }
    private double[] calculateProjectStats(Set<ProjectSupplierEntity> projectSuppliers) {
        double orderQuantity = 0;
        double amountOfOrders = 0;

        for(ProjectSupplierEntity projectSupplier : projectSuppliers){
            for(OrderEntity order : projectSupplier.getOrders()){
                orderQuantity += order.getAmountOrdered();
                amountOfOrders++;
            }
        }
        return new double[]{orderQuantity, amountOfOrders};
    }
    private String getSupplierShortCodes(Set<ProjectSupplierEntity> projectSuppliers){
        String result = "";
        for(ProjectSupplierEntity projectSupplier : projectSuppliers){
            result += projectSupplier.getSupplierEntity().getShortForm() + " ";
        }
        return result.trim().replace(" ", ", ");
    }
    private Supplier convertAddSupplierDtoToSupplier(AddSupplierDto addSupplierDto) {
        Supplier supplier = new Supplier();

        supplier.setShortForm(addSupplierDto.getShortForm());
        supplier.setRegDate(LocalDateTime.now());
        supplier.setActive(true);
        supplier.setCompanyName(addSupplierDto.getCompanyName());
        supplier.setContactName(addSupplierDto.getContactName());
        supplier.setGreetingShort(addSupplierDto.getGreetingShort());
        supplier.setGreetingLong(addSupplierDto.getGreetingLong());
        supplier.setStreet(addSupplierDto.getStreet());
        supplier.setPlz(addSupplierDto.getPlz());
        supplier.setCity(addSupplierDto.getCity());
        supplier.setTelephone(addSupplierDto.getTelephone());
        supplier.setEmail(addSupplierDto.getEmail());
        supplier.setWeb(addSupplierDto.getWeb());
        supplier.setHistory(addSupplierDto.getHistory());

        return supplier;
    }
    private Supplier convertEditSupplierDtoToSupplier(EditSupplierDto editSupplierDto) {
        Supplier supplier = new Supplier();

        supplier.setShortForm(editSupplierDto.getShortForm());
        supplier.setActive(editSupplierDto.isActive());
        supplier.setCompanyName(editSupplierDto.getCompanyName());
        supplier.setContactName(editSupplierDto.getContactName());
        supplier.setGreetingShort(editSupplierDto.getGreetingShort());
        supplier.setGreetingLong(editSupplierDto.getGreetingLong());
        supplier.setStreet(editSupplierDto.getStreet());
        supplier.setPlz(editSupplierDto.getPlz());
        supplier.setCity(editSupplierDto.getCity());
        supplier.setTelephone(editSupplierDto.getTelephone());
        supplier.setEmail(editSupplierDto.getEmail());
        supplier.setWeb(editSupplierDto.getWeb());
        supplier.setHistory(editSupplierDto.getHistory());

        return supplier;
    }
    private SupplierDto convertSupplierEntityToDto(SupplierEntity supplierEntity) {
        SupplierDto supplierDto = new SupplierDto();

        supplierDto.setId(supplierEntity.getId());
        supplierDto.setShortForm(supplierEntity.getShortForm());
        supplierDto.setRegDate(supplierEntity.getRegDate());
        supplierDto.setActive(supplierEntity.isActive());
        supplierDto.setCompanyName(supplierEntity.getCompanyName());
        supplierDto.setContactName(supplierEntity.getContactName());
        supplierDto.setGreetingShort(supplierEntity.getGreetingShort());
        supplierDto.setGreetingLong(supplierEntity.getGreetingLong());
        supplierDto.setStreet(supplierEntity.getStreet());
        supplierDto.setPlz(supplierEntity.getPlz());
        supplierDto.setCity(supplierEntity.getCity());
        supplierDto.setTelephone(supplierEntity.getTelephone());
        supplierDto.setEmail(supplierEntity.getEmail());
        supplierDto.setWeb(supplierEntity.getWeb());
        supplierDto.setHistory(supplierEntity.getHistory());

        return supplierDto;
    }
    private Order convertEditOrderDtoToOrder(EditOrderDto editOrderDto) {
        Order order = new Order();

        order.setMemberId(editOrderDto.getMemberId());
        order.setProjectSupplierId(editOrderDto.getProjectSupplierId());
        if(editOrderDto.getUserId() == 0){
            order.setUserId(1);
        }
        else {
            order.setUserId(editOrderDto.getUserId());
        }
        order.setOrderTypeId(editOrderDto.getOrderTypeId());
        order.setContactTypeId(editOrderDto.getContactTypeId());
        order.setAmountOrdered(editOrderDto.getAmountOrdered());
        order.setWishedDelDate(editOrderDto.getWishedDelDate());
        if(editOrderDto.isMemberConfirmation()) order.setMemberConfirmation(LocalDateTime.now());
        if(editOrderDto.isSupplierConfirmation()) order.setSupplierConfirmation(LocalDateTime.now());
        order.setShortInfo(editOrderDto.getShortInfo());

        return order;
    }
    private Order convertAddOrderDtoToOrder(AddOrderDto addOrderDto) {
        Order order = new Order();

        order.setId(-1);
        order.setMemberId(addOrderDto.getMemberId());
        order.setProjectSupplierId(addOrderDto.getProjectSupplierId());
        order.setUserId(addOrderDto.getUserId());
        order.setOrderTypeId(addOrderDto.getOrderTypeId());
        order.setContactTypeId(addOrderDto.getContactTypeId());
        order.setOrderDate(LocalDateTime.now());
        order.setAmountOrdered(addOrderDto.getAmountOrdered());
        order.setWishedDelDate(addOrderDto.getWishedDelDate());
        order.setShortInfo(addOrderDto.getShortInfo());

        return order;
    }
    private DetailedOrderDto convertOrderEntityToDetailedDto(OrderEntity orderEntity) {
        DetailedOrderDto detailedOrderDto = new DetailedOrderDto();

        detailedOrderDto.setId(orderEntity.getId());
        detailedOrderDto.setMember(convertMemberEntityToShortDto(orderEntity.getMemberEntity()));
        detailedOrderDto.setProjectSupplier(convertProjectSupplierToShortDto(orderEntity.getProjectSupplierEntity()));
        detailedOrderDto.setOrderType(orderEntity.getOrderTypeEntity());
        detailedOrderDto.setContactType(orderEntity.getContactTypeEntity());
        detailedOrderDto.setOrderDate(orderEntity.getOrderDate());
        detailedOrderDto.setAmountOrdered(orderEntity.getAmountOrdered());
        detailedOrderDto.setShortInfo(orderEntity.getShortInfo());
        detailedOrderDto.setAvailableDeliveryDateOptions(getAvailableDeliveryDates(orderEntity.getProjectSupplierEntity().getId()));

        return detailedOrderDto;
    }
    private ShortProjectSupplierDto convertProjectSupplierToShortDto(ProjectSupplierEntity projectSupplierEntity) {
        ShortProjectSupplierDto shortProjectSupplierDto = new ShortProjectSupplierDto();

        shortProjectSupplierDto.setId(projectSupplierEntity.getId());
        shortProjectSupplierDto.setSupplierShortForm(projectSupplierEntity.getSupplierEntity().getShortForm());
        shortProjectSupplierDto.setSupplierCompanyName(projectSupplierEntity.getSupplierEntity().getCompanyName());
        shortProjectSupplierDto.setPelletsPrice(projectSupplierEntity.getPelletsPrice());
        shortProjectSupplierDto.setAlpPrice(projectSupplierEntity.getAlpPrice());
        if(projectSupplierEntity.getDeliveryEnd() != null) shortProjectSupplierDto.setDeliveryEnd(formatDate(projectSupplierEntity.getDeliveryEnd()));

        return shortProjectSupplierDto;
    }
    private String formatDate(Date date){
        String pattern = "MMMMM yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("de", "AT"));
        return simpleDateFormat.format(date);
    }
    private Member convertEditMemberDtoToMember(int memberId, EditMemberDto memberDto) {
        Member member = new Member();

        member.setId(memberId);
        member.setContactTypeId(memberDto.getContactTypeId());
        member.setActive(memberDto.isActive());
        member.setTitle(memberDto.getTitle());
        member.setAdditionalTitle(memberDto.getAdditionalTitle());
        member.setTitle(memberDto.getTitle());
        member.setFirstName(memberDto.getFirstName());
        member.setLastName(memberDto.getLastName());
        member.setStreet(memberDto.getStreet());
        member.setPlz(memberDto.getPlz());
        member.setCity(memberDto.getCity());
        member.setPrivateNumber(memberDto.getPrivateNumber());
        member.setSecondaryNumber(memberDto.getSecondaryNumber());
        member.setEmail(memberDto.getEmail());
        member.setDelAddress(memberDto.getDelAddress());
        member.setPermanentInfo(memberDto.getPermanentInfo());
        member.setInternInfo(memberDto.getInternInfo());
        member.setPassword(memberDto.getPassword());
        member.setWebId(memberDto.getWebId());

        return member;
    }
    private Member convertAddMemberDtoToMember(AddMemberDto addMemberDto) {
        Member member = new Member();

        member.setId(-1);
        member.setContactTypeId(addMemberDto.getContactTypeId());
        member.setRegDate(LocalDateTime.now());
        member.setActive(true);
        member.setTitle(addMemberDto.getTitle());
        member.setAdditionalTitle(addMemberDto.getAdditionalTitle());
        member.setTitle(addMemberDto.getTitle());
        member.setFirstName(addMemberDto.getFirstName());
        member.setLastName(addMemberDto.getLastName());
        member.setStreet(addMemberDto.getStreet());
        member.setPlz(addMemberDto.getPlz());
        member.setCity(addMemberDto.getCity());
        member.setPrivateNumber(addMemberDto.getPrivateNumber());
        member.setSecondaryNumber(addMemberDto.getSecondaryNumber());
        member.setEmail(addMemberDto.getEmail());
        member.setDelAddress(addMemberDto.getDeliveryAddress());
        member.setPermanentInfo(addMemberDto.getPermanentInfo());
        member.setInternInfo(addMemberDto.getInternInfo());
        member.setPassword(createRandomString(8));
        member.setWebId(createRandomString(32));

        return member;
    }
    private String createRandomString(int length){
        String result = "";

        while(result.length() < length){
            char randomChar = (char)((int)(Math.random() * 74) + 48);
            if(!(randomChar >= 58 && randomChar <= 64) && !(randomChar >= 91 && randomChar <= 96)){
                result += randomChar;
            }
        }
        return result;
    }
    private ShortMemberDto convertMemberEntityToShortDto(MemberEntity member) {
        ShortMemberDto shortMemberDto = new ShortMemberDto();

        shortMemberDto.setId(member.getId());
        shortMemberDto.setActive(member.isActive());
        shortMemberDto.setContactType(member.getContactTypeEntity().getType());
        shortMemberDto.setEmail(member.getEmail());
        shortMemberDto.setFullName(member.getLastName() + " " + member.getFirstName());
        shortMemberDto.setPlz(member.getPlz());
        shortMemberDto.setCity(member.getCity());
        shortMemberDto.setStreet(member.getStreet());
        shortMemberDto.setDifferentDeliveryAddress(isDifferentDeliveryAddress(member.getStreet(), member.getPlz(), member.getCity(), member.getDelAddress()));
        shortMemberDto.setPrivateNumber(member.getPrivateNumber());
        shortMemberDto.setSecondaryNumber(member.getSecondaryNumber());
        shortMemberDto.setRegDate(member.getRegDate());

        return shortMemberDto;
    }

    private boolean isDifferentDeliveryAddress(String street, String plz, String city, String deliveryAddress){
        String address = String.format("%s %s, %s", plz, city, street);

        if(deliveryAddress != null && !address.equalsIgnoreCase(deliveryAddress)){
            return true;
        }
        return false;
    }
    private DetailedMemberDto convertMemberEntityToDetailedMemberDto(MemberEntity memberEntity){
        DetailedMemberDto member = new DetailedMemberDto();

        member.setId(memberEntity.getId());
        member.setActive(memberEntity.isActive());
        if(memberEntity.getOrders() != null) member.setOrders(convertOrderEntitiesToShortDto(memberEntity.getOrders()));
        member.setCity(memberEntity.getCity());
        member.setContactType(memberEntity.getContactTypeEntity());
        member.setEmail(memberEntity.getEmail());
        member.setAdditionalTitle(memberEntity.getAdditionalTitle());
        member.setDelAddress(memberEntity.getDelAddress());
        member.setPlz(memberEntity.getPlz());
        member.setFirstName(memberEntity.getFirstName());
        member.setLastName(memberEntity.getLastName());
        member.setPrivateNumber(memberEntity.getPrivateNumber());
        member.setSecondaryNumber(memberEntity.getSecondaryNumber());
        member.setTitle(memberEntity.getTitle());
        member.setRegDate(memberEntity.getRegDate());
        member.setStreet(memberEntity.getStreet());
        member.setPermanentInfo(memberEntity.getPermanentInfo());
        member.setInternInfo(memberEntity.getInternInfo());
        member.setPassword(memberEntity.getPassword());
        member.setWebId(memberEntity.getWebId());

        return member;
    }
    private List<ShortOrderDto> convertOrderEntitiesToShortDto(Set<OrderEntity> orders) {
        List<ShortOrderDto> shortOrderDtos = new ArrayList<>();

        for (OrderEntity orderEntity : orders) {
            shortOrderDtos.add(convertOrderEntityToShortDto(orderEntity));
        }
        return shortOrderDtos;
    }
    private ShortOrderDto convertOrderEntityToShortDto(OrderEntity orderEntity) {
        ShortOrderDto shortOrderDto = new ShortOrderDto();

        shortOrderDto.setId(orderEntity.getId());
        shortOrderDto.setUsername(orderEntity.getUserEntity().getShortForm());
        shortOrderDto.setContactType(orderEntity.getContactTypeEntity().getType());
        shortOrderDto.setWishedDelDate(orderEntity.getWishedDelDate());
        shortOrderDto.setMemberConfirmation(orderEntity.getMemberConfirmation() == null ? false : true);
        shortOrderDto.setSupplierConfirmation(orderEntity.getSupplierConfirmation() == null ? false : true);
        shortOrderDto.setMember(convertMemberEntityToShortDto(orderEntity.getMemberEntity()));
        shortOrderDto.setAmountOrdered(orderEntity.getAmountOrdered());
        shortOrderDto.setFile(orderEntity.getFile());
        shortOrderDto.setSupplierShortForm(orderEntity.getProjectSupplierEntity().getSupplierEntity().getShortForm());

        return shortOrderDto;
    }
    //endregion
}