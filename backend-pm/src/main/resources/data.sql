INSERT INTO contacttype (type) VALUES ('Intern');
INSERT INTO contacttype (type) VALUES ('Brief');
INSERT INTO contacttype (type) VALUES ('FAX');
INSERT INTO contacttype (type) VALUES ('Mail');
INSERT INTO contacttype (type) VALUES ('Online');
INSERT INTO contacttype (type) VALUES ('Kontakt+');

INSERT INTO ordertype (type) VALUES ('Keine Rückmeldung');
INSERT INTO ordertype (type) VALUES ('Kein Bedarf');
INSERT INTO ordertype (type) VALUES ('Standard');
INSERT INTO ordertype (type) VALUES ('Dringend');

INSERT INTO member (active, additional_title, city, del_address, email, first_name, intern_info, last_name, password, permanent_info, plz, private_number, reg_date, secondary_number, street, title, web_id, message_type)
VALUES (1, 'Master', 'Pattigham', '4910 Ried im Innkreis, Lärchenstraße 69', 'myemail@domain.at', 'Peter', 'Info über Peter', 'Kaiser', 'öpd87', 'Enge Zufahrt, Ohne Anhänger', '4910', '0677/589658', '2022-07-13 00:00:00', '07754/8701-6932', 'Schoberstraße 36', 'Dr. Dipl. Ing.', 'lkasjdlqwkejrl91823', 4);

INSERT INTO supplier (active, city, company_name, contact_name, email, greeting_long, greeting_short, history, plz, reg_date, short_form, street, telephone, web)
VALUES (1, 'Pattigham', 'Maier und Korduletsch GmbH', 'Julian-Gernot Voggenreither', 'jgvoggenreiter@mail.at', 'Sehr geehrter Herr', 'Herr', 'Informationen über ihn', '49101', '2022-02-24 00:00:00', 'MaKo', 'Schoberstraße 66', '+49 0677/589666', 'https://website.de');

INSERT INTO project (active, end_order_date, expenses, history, login_prefix, path, ppi_date, ppi_price, ppi_value, quantity_orders_previous_year, quantity_orders_previous_year0, quantity_previous_year, short_description)
VALUES (1, '2023-07-01 00:00:00', 10.20, 'Informationen und so', 'dw', '2022', '2022-05-25 00:00:00', 260.36, 116.36, 1859, 210, 11556.66, 'Einlagerung 2022');

INSERT INTO projectsupplier (project_id, supplier_id, alp_price, order_quantity, amount_of_orders, delivery_end, min_delivery_amount, pellets_price)
VALUES (1, 1, 47, 0, 0, '2022-09-30 00:00:00', 3000, 236.33);

INSERT INTO user (active, creation_date, first_name, last_name, password, short_form)
VALUES (1, '2022-07-13 00:00:00', 'Kaiser', 'Peter', 'kape', 'KaPe');

INSERT INTO orders (user_id, supplier_id, member_id, contacttype_id, ordertype_id, amount_ordered, file, order_date, short_info, wished_del_date)
VALUES (1, 1, 1, 4, 3, 80000, 'Be233345.pdf', '2022-07-12 14:34:59', 'Nur vormittags liefern, telefonische Rückmeldung', 'September 2022');

INSERT INTO logging (project_id, orders_id, member_id, message_type_id, log_date, log_type, message_receiver, message_status)
VALUES (1, 1, 1, 4, '2022-07-13 14:30:44', 'Bestellbestätigung', 'email@domain.at', 'OK');